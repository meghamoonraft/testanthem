$(function () {
    let hostList = ["www.anthem.com",
      "www.empireblue.com",
      "www.communitycareexplorer.com",
      "anthem-dev3.adobecqms.net",
      "anthem-qa1.adobecqms.net",
      "anthem-uat2.adobecqms.net",
      "anthem-stage.adobecqms.net",
      "anthem-prod1.adobecqms.net"];

    let abcHostList = ["https://www.anthem.com/ca",
      "https://anthem-dev3.adobecqms.net/ca",
      "https://anthem-qa1.adobecqms.net/ca",
      "https://anthem-uat2.adobecqms.net/ca",
      "https://anthem-stage.adobecqms.net/ca",
      "https://anthem-prod1.adobecqms.net/ca"];

    $(".cmp-navigation__item--level-1 .cmp-navigation__item-link").each(
      function () {
        let path = $(this).attr("href");
        let currentURL = window.location.href;
        if (hostList.indexOf(window.location.hostname) >= 0) {
          if (path.indexOf("/content") == 0) {
            if (checkInput(currentURL, abcHostList)) {
              path = "/ca/tng" + path.split("/tng").pop();
            } else {
              path = "/tng" + path.split("/tng").pop();
            }
          }
        }
        $(this).attr("href", path);
        // Reset the navigation component for desktop view
        $(window).resize(function () {
          var screenSize = $(window).width();
          if (screenSize > 767) {
            $(".navigation").show();
          }
        });
      }
    );

    function checkInput(input, words) {
      return words.some(word => input.toLowerCase().includes(word.toLowerCase()));
    }
  });