$(function () {
  var $searchInput = $(
    ".vacnSearch .vacnSearch__wrap .vacnSearch__input .vacnSearch__input__searchBox .vacnSearch__input__searchBox__text"
  ); // DOM Instance for input box
  var $searchList = $(
    ".vacnSearch .vacnSearch__wrap .vacnSearch__input .vacnSearch__input__list"
  ); // DOM Instance for suggestion list
  var $distanceFilter = $(".resultsFilter__dist .resultsFilter__select"); // DOM Instance for distance filter button
  var $distanceFilterItem = $(
    ".resultsFilter__dist .resultsFilter__list .filterList__item"
  ); //DOM instance for filter list item
  var $distanceFilterList = $(".resultsFilter__list"); //DOM instance for filter
  var $resultSummary = $(".search_result .search_summary"); // DOM Instance for result summary details
  var $listTileWrap = $(".search_wrap .vacnTileWrap"); //DOM instance for List view wrapper
  var $listTile = $(".search_wrap .vacnTileWrap .vacnTile"); //DOM instance for List view
  var $showHide = $(".vacnTileWrap .vacTile__view"); // DOM Instance for view more button
  var $viewType = $(".search_wrap .resultsInfo_view_info"); // DOM Instance for view type button
  var $mapView = $(".search_wrap .vacnMap"); //DOM instance for Map view
  var $mapListView = $(".search_wrap .vacnMap .vacnMap__view__list"); //DOM instance for Map list view
  var $defaultView = $(".search_intro"); // DOM instance for search intro section
  var $searchResult = $(".search_wrap"); // DOM instance for search container
  var $loader = $(".loader"); // DOM instance for loader section
  var $noResults = $(".search_wrap .no_results_section"); //DOM instance for no results section
  var $cloudError = $(".cloud_error_info"); //DOM instance for cloud error info section
  var selectedListItemIndex = -1; // Variable to store the li index value which is getting focused from Autosuggestion list

  // config for API and to store the selected user location and data
  var config = {
    baseUrl: "https://dev.virtualearth.net/REST/v1",
    apiKey: "AnUE73t9h-Fg9gM2QdN9JrzlRvlQlI2zaEDviBrZB64gXL77GUpxIM6Nbj78q6TG",
    vaccineDataAPI: "/content/covid-sites/api/vaccine-site-finder",
    selectedLocation: {
      formattedAddress: null,
      coordinates: null,
      adminDistrict: null,
      distanceThreshold: 25,
      distanceUnit: "miles",
    },
    vaccineData: [],
    paginationLimit: 5,
    selectedType: "Address,Place",
    countryCode: "US",
    maxResults: 5,
  };
  resetPage(); //Reset the page while loading

  // Function to close the autosuggestion box while clicking outside the box
  $(document).on("click", function () {
    // hiding the auto suggestion list
    $searchList.hide();
    // hiding the distance filter list
    $distanceFilterList.css("display") == "block"
      ? $distanceFilter.children(".dropdown").toggleClass("dropdown--rot")
      : "";
    $distanceFilterList.hide();
  });

  // Function to handle the keyboard navigation through the autosuggestion list items
  $(document).on("keydown", function (event) {
    try {
      switch (event.key) {
        case "ArrowDown":
          event.preventDefault();
          selectedListItemIndex =
            selectedListItemIndex + 1 < config.maxResults
              ? selectedListItemIndex + 1
              : 0;
          $(".vacnSearch__input__list__item")[selectedListItemIndex].focus();
          break;
        case "ArrowUp":
          event.preventDefault();
          selectedListItemIndex =
            selectedListItemIndex > 0
              ? selectedListItemIndex - 1
              : config.maxResults - 1;
          $(".vacnSearch__input__list__item")[selectedListItemIndex].focus();
          break;
        case "Enter":
          $(".vacnSearch__input__list__item")[selectedListItemIndex].click();
          selectedListItemIndex = 0;
        default:
          return;
      }
    } catch (e) {}
  });

  // Function to handle view more functionality
  $showHide.on("click", function () {
    getVaccineData();
    // Hiding the show more button if all the cards are rendered
    $listTile.find(".search_result_item_container").length ==
    config.vaccineData.length
      ? $showHide.hide()
      : $showHide.show();
  });

  // Function to generate the auto suggestion list
  $searchInput.on("keyup", function (event) {
    event.stopPropagation();
    // reset the page
    resetPage();
    // Make API call only if the input box is not empty
    if ($searchInput.val().trim().length > 0) {
      $.ajax({
        type: "GET",
        dataType: "json",
        url: config.baseUrl + "/AutoSuggest",
        data: {
          key: config.apiKey,
          q: $searchInput.val(),
          inclenttype: config.selectedType,
          cf: config.countryCode,
        },
        // callback function to execute if the API call gets executed successfully
        success: function (data) {
          var result = data.resourceSets[0];
          if (result) {
            if (
              result.estimatedTotal > 0 &&
              result.resources[0].value.length > 0
            ) {
              // empty the search suggestion list and append the new set of suggestions
              $searchList.empty();
              $.map(result.resources[0].value, function (item, index) {
                if (index < config.maxResults) {
                  $searchList.append(
                    '<li class="vacnSearch__input__list__item" tabIndex="0"><div class="vacnSearch__input__list__item__logo"><span class="motif-icon motif-marker"></span></div><div class="vacnSearch__input__list__item__info"><h6>'
                      .concat(
                        item.address.locality ? item.address.locality : "",
                        '</h6><span id="formattedAddress">'
                      )
                      .concat(
                        item.address.formattedAddress
                          ? item.address.formattedAddress
                          : "",
                        "</span></div></li>"
                      )
                  );
                  $searchList.show();
                  $distanceFilterList.hide();
                }
              });
              $(".vacnSearch__input__list__item").on(
                "click",
                getSelectedLocation
              );
            } else {
              $searchList.hide();
            }
          }
        },
      });
    } else {
      resetPage();
    }
  });

  // Function to handle the distance filter selection
  $distanceFilter.on("click", function (event) {
    event.stopPropagation();
    $(this).children(".dropdown").toggleClass("dropdown--rot");
    $distanceFilterList.toggle();
    $searchList.hide();
  });

  // Function to handle the selection of distance filter
  $distanceFilterItem.on("click", function (event) {
    event.stopPropagation();
    $(this).siblings().removeClass("focus");
    $(this).addClass("focus");
    $distanceFilter
      .children(".resultsFilter__info")
      .text("Within ".concat($(this).attr("value"), " Miles"));
    config.selectedLocation.distanceThreshold = $(this).attr("value");
    $distanceFilter.trigger("click");
    if (config.selectedLocation.formattedAddress != null) {
      $loader.show();
      fetchVaccineSiteData();
    }
  });

  // Function to reset the user's selected location
  function resetPage() {
    config.selectedLocation = {
      formattedAddress: null,
      coordinates: null,
      adminDistrict: null,
      distanceThreshold: 25,
      distanceUnit: "miles",
    };
    $searchList.empty();
    $searchList.hide();
    $distanceFilterList.hide();
    // set the default scroll start value to top
    $mapListView.scrollTop(0);
    $defaultView.show();
    $searchResult.hide();
    $loader.hide();
    $showHide.show();
    $mapView.hide();
    $noResults.hide();
    $cloudError.hide();
    $listTileWrap.hide();
    $mapListView
      .children(".search_result_map_view")
      .removeClass("vacnMap__selected__list");
    $distanceFilter.children(".resultsFilter__info").text("Within 25 Miles");
    // select the default filter value for each search
    $distanceFilterItem.map(function () {
      $(this).attr("value") == config.selectedLocation.distanceThreshold
        ? $(this).addClass("focus")
        : $(this).removeClass("focus");
    });
    $viewType.removeClass("activeView");
    $viewType.last().addClass("activeView");
  }

  // Function to toggle between map view and list view
  $viewType.on("click", function () {
    $(this).addClass("activeView");
    $(this).siblings().removeClass("activeView");
    switch ($(this).text().trim()) {
      case "Map View":
        $listTileWrap.hide();
        $mapView.css("display", "flex");
        handleErrorScenario("Map View");
        break;
      case "List View":
        $mapView.hide();
        $listTileWrap.show();
        handleErrorScenario("List View");
        break;
      default:
        return;
    }
  });

  // Function to display error message if data is not available
  function handleErrorScenario(type) {
    if (config.vaccineData.length <= 0) {
      $showHide.hide();
      $listTile.hide();
      $noResults.show();
      $mapView.hide();
      $mapListView.hide();
      $noResults
        .find(".search_empty_result_text")
        .text(
          "Sorry, no results were found within ".concat(
            config.selectedLocation.distanceThreshold,
            " miles of ".concat(
              config.selectedLocation.formattedAddress,
              ". Please modify the location or search radius."
            )
          )
        );
    } else {
      $noResults.hide();
      type === "Map View"
        ? (function () {
            $mapView.find("#vacnMap__view").show();
            $mapListView.show();
            $mapView.css("display", "flex");
            $listTileWrap.hide();
          })()
        : (function () {
            $listTile.show();
            $mapView.hide();
            $listTileWrap.show();
          })();
    }
    $searchResult.show();
  }

  // Function to get the zip code or coordinates of the selected location
  function getSelectedLocation() {
    config.selectedLocation.formattedAddress = $(this)
      .children(".vacnSearch__input__list__item__info")
      .children("#formattedAddress")
      .text();
    $searchInput.val(config.selectedLocation.formattedAddress);
    $loader.show();
    getLocationCoordinates(config.selectedLocation.formattedAddress);
    $searchList.hide();
  }

  // Util Function to convert the selected location to coordinates
  function getLocationCoordinates(locationValue) {
    $.ajax({
      type: "GET",
      dataType: "json",
      url: config.baseUrl + "/Locations",
      data: {
        key: config.apiKey,
        q: locationValue,
        maxResults: 1,
      },
      success: function (data) {
        config.selectedLocation.coordinates = {
          latitude:
            data.resourceSets[0].resources[0].geocodePoints[0].coordinates[0],
          longitude:
            data.resourceSets[0].resources[0].geocodePoints[0].coordinates[1],
        };
        config.selectedLocation.adminDistrict =
          data.resourceSets[0].resources[0].address.adminDistrict;
        fetchVaccineSiteData();
      },
    });
  }

  // Function to fetch the vaccine site data for the user selected location
  function getVaccineData() {
    if (config.vaccineData.length > 0) {
      // Variable to store the number of list items visible
      var existingListLength = $listTile.find(".search_result_item_container")
        .length;
      for (
        var iterator = existingListLength;
        iterator < existingListLength + config.paginationLimit &&
        iterator < config.vaccineData.length;
        iterator++
      ) {
        $listTile.append(
          returnListTemplate(config.vaccineData[iterator], false, iterator)
        );
      }
      // Handle the visibility of view more button
      $listTile.find(".search_result_item_container").length ==
      config.vaccineData.length
        ? $showHide.hide()
        : $showHide.show();
    }
    handleErrorScenario($(".activeView").text().trim());
    $resultSummary.html(
      "<b>".concat(
        config.vaccineData.length,
        "</b> ".concat(
          config.vaccineData.length == 1 ? "result" : "results",
          " within<b> ".concat(
            config.selectedLocation.distanceThreshold,
            " ".concat(
              config.selectedLocation.distanceUnit,
              " </b> of <b> ".concat(
                config.selectedLocation.formattedAddress,
                "</b>"
              )
            )
          )
        )
      )
    );
    // hide the loader and displayed the results
    $loader.hide();
  }

  // Function to fetch the vaccine site data from AEM server
  function fetchVaccineSiteData() {
    $.ajax({
      type: "POST",
      url: config.vaccineDataAPI,
      headers: {
        "Content-Type": "application/json",
      },
      dataType: "json",
      data: JSON.stringify({
        state: config.selectedLocation.adminDistrict.toString(),
        county: "",
        latitude: config.selectedLocation.coordinates.latitude.toString(),
        longitude: config.selectedLocation.coordinates.longitude.toString(),
        distanceThreshold: config.selectedLocation.distanceThreshold.toString(),
      }),
      // callback function to execute if the API call gets executed successfully
      success: function success(data) {
        // To avoid inconsistency issue with response, added a condition to parse the string
        config.vaccineData =
          typeof data == "string" ? JSON.parse(data["result"]) : data["result"];
        // function to render the results
        $defaultView.hide();
        // function to clear the existing list and render the new set of lists
        $listTile.empty();
        // function to clear the existing list in map view and render the new set of lists
        $mapListView.empty();
        // function to re-render the map view
        loadMapView();
        // function render the list
        getVaccineData();
      },
      // callback function to execute if the API call gets failed
      error: function error() {
        $cloudError.show();
        $defaultView.hide();
        $searchResult.hide();
      },
    });
  }

  // Function to render a single card based on the data user is passing
  function returnListTemplate(vaccineData, isAlignVertical, iterator) {
    return '<div class="search_result_item_container '.concat(
      isAlignVertical ? 'search_result_map_view"' : '"',
      'id="vaccine__data__'.concat(
        iterator,
        '"><div class="name_address_section"><div class="name_section_text"><a rel="noopener noreferrer" target="_blank"'
          .concat(vaccineData.URL ? "href= '" + vaccineData.URL + "'" : "", ">")
          .concat(
            vaccineData.Name ? vaccineData.Name : "",
            '</a></div><div class="address_section_text">'
          )
          .concat(
            vaccineData.Address ? vaccineData.Address : "",
            '<div class="address_section_miles"><span class="motif-icon motif-marker loc__marker"></span><span>'.concat(
              vaccineData.distance ? vaccineData.distance.toFixed(2) : "",
              ' miles away </span></div></div></div><a class="contact_section" href="tel:+'.concat(
                vaccineData.Phone ? vaccineData.Phone : "",
                '">'
              )
            )
          )
          .concat(
            vaccineData.Phone ? vaccineData.Phone : "",
            '</a><div class="button_section"><a rel="noopener noreferrer" target="_blank"'
          )
          .concat(vaccineData.URL ? "href= '" + vaccineData.URL + "'" : "", ">")
          .concat(
            '<button class="visit_web_btn"'.concat(
              vaccineData.URL ? "" : "disabled",
              '><span class="motif-icon motif-external-link"></span>'
            )
          )
          .concat("View Website", "</button></a></div></div>")
      )
    );
  }

  // Function to load the map pins
  function loadMapView() {
    // Generating the map with user selected location as the center point
    var map = new Microsoft.Maps.Map(document.getElementById("vacnMap__view"), {
      credentials: config.apiKey,
      mapTypeId: Microsoft.Maps.MapTypeId.road,
      center: new Microsoft.Maps.Location(
        parseFloat(config.selectedLocation.coordinates.latitude),
        parseFloat(config.selectedLocation.coordinates.longitude)
      ),
      zoom: 14.5,
    });
    //Create custom Pushpin
    var customPin = new Microsoft.Maps.Pushpin(map.getCenter(), {
      icon: "/content/dam/covid19/map-user-marker.png",
      anchor: new Microsoft.Maps.Point(12, 39),
    });
    //Create an infobox at the center of the map but don't show it.
    infobox = new Microsoft.Maps.Infobox(map.getCenter(), {
      visible: false,
    });
    //Assign the infobox to a map instance.
    infobox.setMap(map);
    // Creates a collection to store multiple pins
    var pins = new Microsoft.Maps.EntityCollection();
    pins.push(customPin);
    // Creates random pins
    for (var i = 0; i < config.vaccineData.length; i++) {
      // A random position
      var position = new Microsoft.Maps.Location(
        parseFloat(config.vaccineData[i].Latitude),
        parseFloat(config.vaccineData[i].Longitude)
      );
      // Creates a Pushpin
      var pin = new Microsoft.Maps.Pushpin(position);
      pin.metadata = {
        description: returnListTemplate(config.vaccineData[i], true),
        id: i,
      };
      //Add a click event handler to the pushpin.
      Microsoft.Maps.Events.addHandler(pin, "click", pushpinClicked);
      // Adds the pin to the collection instead of adding it directly to the Map
      pins.push(pin);
    }
    // Adds all pins at once
    map.entities.push(pins);
    // Function to load the list data for map view
    getVaccineMapViewList();
    $loader.hide();
  }

  // Function to load the list data for mobile view
  function getVaccineMapViewList() {
    if (config.vaccineData.length > 0) {
      $mapListView.empty();
      $.map(config.vaccineData, function (vaccineData, iterator) {
        $mapListView.append(returnListTemplate(vaccineData, true, iterator));
      });
    }
    handleErrorScenario($(".activeView").text().trim());
  }
  // Function to open the custom info box for each pin in map
  function pushpinClicked(event) {
    //Make sure the infobox has metadata to display.
    if (event.target.metadata) {
      //Set the infobox options with the metadata of the pushpin.
      infobox.setOptions({
        location: event.target.getLocation(),
        description: event.target.metadata.description,
        visible: true,
      });
    }
    moveToSelectedListData("vaccine__data__" + event.target.metadata.id);
  }

  // function to auto scroll the map list data while clicking the particular pin
  function moveToSelectedListData(dataId) {
    // remove the highlighter from the unselected map pin
    $mapListView
      .children(".search_result_map_view")
      .removeClass("vacnMap__selected__list");
    // add the highlighter for the unselected map pin
    $(".search_result_map_view#" + dataId).addClass("vacnMap__selected__list");
    // calculating the new scroll position
    var position =
      $(".vacnMap__selected__list").offset().top -
      $mapListView.offset().top +
      $mapListView.scrollTop();
    $mapListView.animate({ scrollTop: position, speed: "slow" });
  }
});
