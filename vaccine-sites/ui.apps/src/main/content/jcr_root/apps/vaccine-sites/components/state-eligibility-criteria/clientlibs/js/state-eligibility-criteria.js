$(function () {
  // Hardcoded value for list of states
  var stateLinks = [
    {
      state: "Alabama",
      linkText: "Alabama Department of Public Health: COVID-19 Vaccine",
      link: "https://www.alabamapublichealth.gov/covid19vaccine/index.html",
    },
    {
      state: "Alaska",
      linkText:
        "Alaska Department of Health and Social Services: COVID-19 Vaccine Status",
      link: "http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx",
    },
    {
      state: "Arizona",
      linkText: "Arizona Department of Health Services: COVID-19 Vaccines",
      link:
        "https://azdhs.gov/preparedness/epidemiology-disease-control/infectious-disease-epidemiology/index.php#novel-coronavirus-vaccine",
    },
    {
      state: "Arkansas",
      linkText: "Arkansas Department of Health: COVID-19 Vaccination Plan",
      link:
        "https://www.healthy.arkansas.gov/programs-services/topics/covid-19-vaccination-plan",
    },
    {
      state: "California",
      linkText:
        "Official California State Government COVID-19 Website: Vaccines",
      link: "https://covid19.ca.gov/vaccines/",
    },
    {
      state: "Colorado",
      linkText:
        "Colorado Department of Public Health and Environment: Vaccine for Coloradans",
      link:
        "https://covid19.colorado.gov/for-coloradans/vaccine/vaccine-for-coloradans",
    },
    {
      state: "Connecticut",
      linkText: "Connecticut's Official State Website: COVID-19 Vaccine",
      link: "https://portal.ct.gov/Coronavirus/covid-19%20vaccinations",
    },
    {
      state: "Delaware",
      linkText: "Delaware Division of Public Health: COVID-19 Vaccine",
      link: "https://coronavirus.delaware.gov/vaccine/",
    },
    {
      state: "District of Columbia",
      linkText:
        "Government of the District of Columbia: Coronavirus (COVID-19) Vaccine",
      link: "https://coronavirus.dc.gov/vaccine",
    },
    {
      state: "Florida",
      linkText: "Florida Health: COVID-19 Vaccines in Florida",
      link: "https://floridahealthcovid19.gov/covid-19-vaccines-in-florida/",
    },
    {
      state: "Georgia",
      linkText: "Georgia Department of Public Health: COVID-19 Vaccines",
      link: "https://dph.georgia.gov/covid-vaccine",
    },
    {
      state: "Hawai'i",
      linkText: "State of Hawai'i Portal: COVID-19 Vaccination Overview",
      link: "https://hawaiicovid19.com/vaccine/",
    },
    {
      state: "Idaho",
      linkText:
        "Idaho Official Resources for Novel Coronavirus: COVID-19 Vaccine",
      link: "https://coronavirus.idaho.gov/covid-19-vaccine/",
    },
    {
      state: "Illinois",
      linkText:
        "State of Illinois COVID-19 Response: Vaccination Plan Overview",
      link: "https://coronavirus.illinois.gov/s/vaccine-plan-overview",
    },
    {
      state: "Indiana",
      linkText: "Indiana Government Website: Vaccine Information and Planning",
      link: "https://www.coronavirus.in.gov/vaccine/index.htm",
    },
    {
      state: "Iowa",
      linkText: "COVID-19 in Iowa: Vaccine Information",
      link: "https://coronavirus.iowa.gov/pages/vaccineinformation",
    },
    {
      state: "Kansas",
      linkText:
        "Kansas Department of Health and Environment: COVID-19 Vaccine Information",
      link: "https://www.kansasvaccine.gov/",
    },
    {
      state: "Kentucky",
      linkText:
        "Kentucky Cabinet for Health and Family Services: COVID-19 Vaccine",
      link: "https://govstatus.egov.com/ky-covid-vaccine",
    },
    {
      state: "Louisiana",
      linkText:
        "Louisiana Department of Health: COVID-19 Vaccination Information",
      link: "https://ldh.la.gov/covidvaccine/",
    },
    {
      state: "Maine",
      linkText: "State of Maine: COVID-19 Vaccination in Maine",
      link: "https://www.maine.gov/covid19/vaccines",
    },
    {
      state: "Maryland",
      linkText: "Maryland Department of Health: COVID-19 Vaccine",
      link: "https://covidlink.maryland.gov/content/vaccine/",
    },
    {
      state: "Massachusetts",
      linkText: "Commonwealth of Massachusetts: COVID-19 Vaccine",
      link: "https://www.mass.gov/covid-19-vaccine",
    },
    {
      state: "Michigan",
      linkText: "State of Michigan: COVID-19 Vaccine",
      link:
        "https://www.michigan.gov/coronavirus/0,9753,7-406-98178_103214_104822---,00.html",
    },
    {
      state: "Minnesota",
      linkText: "Minnesota COVID-19 Response: COVID-19 Vaccinations",
      link: "https://mn.gov/covid19/vaccine/index.jsp",
    },
    {
      state: "Mississippi",
      linkText:
        "Mississippi State Department of Health: Vaccination Against COVID-19",
      link: "https://msdh.ms.gov/msdhsite/_static/14,0,420,976.html",
    },
    {
      state: "Missouri",
      linkText:
        "Missouri Department of Health & Senior Services: COVID-19 Vaccine",
      link: "https://covidvaccine.mo.gov/",
    },
    {
      state: "Montana",
      linkText:
        "Montana Department of Public Health and Human Services: COVID-19 Vaccination Information",
      link: "https://dphhs.mt.gov/covid19vaccine",
    },
    {
      state: "Nebraska",
      linkText:
        "Nebraska Department of Health & Human Services: COVID-19 Vaccine Information",
      link: "https://dhhs.ne.gov/Pages/COVID-19-Vaccine-Information.aspx",
    },
    {
      state: "Nevada",
      linkText: "Nevada Health Response: COVID-19 Vaccine",
      link: "https://nvhealthresponse.nv.gov/covid-19-vaccine/",
    },
    {
      state: "New Hampshire",
      linkText:
        "New Hampshire Department of Health and Human Services: COVID-19 Vaccine Information",
      link:
        "https://www.nh.gov/covid19/resources-guidance/vaccination-planning.htm",
    },
    {
      state: "New Jersey",
      linkText: "New Jersey COVID Information Hub: COVID-19 Vaccine",
      link: "https://covid19.nj.gov/pages/vaccine",
    },
    {
      state: "New Mexico",
      linkText: "New Mexico Department of Health: COVID-19 Vaccine",
      link: "https://cv.nmhealth.org/covid-vaccine/",
    },
    {
      state: "New York",
      linkText: "New York State: COVID-19 Vaccine",
      link: "https://covid19vaccine.health.ny.gov/",
    },
    {
      state: "North Carolina",
      linkText:
        "North Carolina Department of Health and Human Services: COVID-19 Vaccine Information",
      link: "https://covid19.ncdhhs.gov/vaccines",
    },
    {
      state: "North Dakota",
      linkText: "North Dakota Health: COVID-19 Vaccine Information",
      link: "https://www.health.nd.gov/covid-19-vaccine-information",
    },
    {
      state: "Ohio",
      linkText: "Ohio Department of Health: COVID-19 Vaccination Program",
      link:
        "https://coronavirus.ohio.gov/wps/portal/gov/covid-19/covid-19-vaccination-program",
    },
    {
      state: "Oklahoma",
      linkText: "Oklahoma State Department of Health: COVID-19 Vaccine Phases",
      link: "https://oklahoma.gov/covid19/vaccine-information.html",
    },
    {
      state: "Oregon",
      linkText: "Oregon Health Authority: COVID-19 Vaccine",
      link: "https://covidvaccine.oregon.gov/",
    },
    {
      state: "Pennsylvania",
      linkText: "Pennsylvania Department of Health: COVID-19 Vaccines",
      link:
        "https://www.health.pa.gov/topics/disease/coronavirus/Vaccine/Pages/Vaccine.aspx",
    },
    {
      state: "Rhode Island",
      linkText:
        "Rhode Island Department of Health: COVID-19 Vaccine Information",
      link: "https://covid.ri.gov/vaccination",
    },
    {
      state: "South Carolina",
      linkText:
        "South Carolina Department of Health and Environmental Control: COVID-19 Vaccine",
      link: "https://scdhec.gov/covid19/covid-19-vaccine",
    },
    {
      state: "South Dakota",
      linkText:
        "South Dakota Department of Health: COVID-19 Vaccine Information",
      link: "https://doh.sd.gov/COVID/Vaccine/Public.aspx",
    },
    {
      state: "Tennessee",
      linkText:
        "State of Tennessee Government Website: COVID-19 Vaccine Phases",
      link: "https://covid19.tn.gov/covid-19-vaccines/vaccine-phases/",
    },
    {
      state: "Texas",
      linkText:
        "Texas Department of State Health Services: COVID-19 Vaccine Information",
      link: "https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx",
    },
    {
      state: "Utah",
      linkText:
        "State of Utah Government Website: COVID-19 Vaccine Information",
      link: "https://coronavirus.utah.gov/vaccine",
    },
    {
      state: "Vermont",
      linkText: "Vermont Department of Health: COVID-19 Vaccine",
      link: "https://www.healthvermont.gov/covid-19/vaccine",
    },
    {
      state: "Virginia",
      linkText: "Virginia Department of Health: COVID-19 Vaccination Response",
      link: "https://www.vdh.virginia.gov/covid-19-vaccine/",
    },
    {
      state: "Washington",
      linkText: "Washington State Department of Health: COVID-19 Vaccine",
      link: "https://www.doh.wa.gov/Emergencies/COVID19/Vaccine",
    },
    {
      state: "West Virginia",
      linkText:
        "West Virginia Department of Health and Human Resources: COVID-19 Vaccine",
      link: "https://dhhr.wv.gov/COVID-19/Pages/Vaccine.aspx",
    },
    {
      state: "Wisconsin",
      linkText: "Wisconsin Department of Health Services: COVID-19 Vaccine",
      link: "https://www.dhs.wisconsin.gov/covid-19/vaccine.htm",
    },
    {
      state: "Wyoming",
      linkText: "Wyoming Department of Health: COVID-19 Vaccine Information",
      link:
        "https://health.wyo.gov/publichealth/immunization/wyoming-covid-19-vaccine-information/",
    },
  ];

  // Iterate through each state and render that in html
  $.map(stateLinks, function (value) {
    $(".vaccine__state__link__list").append(
      '<div class="vaccine__state__link__block"><div class="vaccine__state__name">' +
        value.state +
        '</div><div class="vaccine__state__link"><a class="vaccine__state__link__text" target="_blank" rel="noopener noreferrer" href=" ' +
        value.link +
        ' "> ' +
        value.linkText +
        '</a><span class="motif-icon motif-external-link"></span></div></div>'
    );
  });
});
