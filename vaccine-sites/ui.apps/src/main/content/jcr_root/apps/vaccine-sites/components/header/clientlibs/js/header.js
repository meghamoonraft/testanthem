$(function () {
	var $hamburger = $(".hamburger__menu"); //DOM instance for hamburger menu
	var $navigation = $(".navigation"); //DOM instance for navigation
	var $headerLogo = $(".anthem__logo"); // DOM instance for setting the custom logo

	$hamburger.on("click", function () {
		$hamburger.children("#menu__icon").toggleClass("motif-menu");
		$hamburger.children("#menu__icon").toggleClass("motif-delete");
		$navigation.css("display") === "block" ?
			$navigation.hide() :
			$navigation.show();
	});

    if($headerLogo){
	let currentURL = window.location.href;
	if (currentURL.indexOf("https://www.anthem.com/ca") == 0 || currentURL.indexOf("https://anthem-dev3.adobecqms.net/ca") == 0 || currentURL.indexOf("https://anthem-qa1.adobecqms.net/ca") == 0 || currentURL.indexOf("https://anthem-uat2.adobecqms.net/ca") == 0 || currentURL.indexOf("https://anthem-stage.adobecqms.net/ca") == 0 || currentURL.indexOf("https://anthem-prod1.adobecqms.net/ca") == 0) {
		$headerLogo.attr('href', "/ca/tng/covid19/vaccine-site-finder.html");
		$headerLogo.find("img").attr('src', "/content/dam/covid19/anthem-abc-logo.svg");
	} else {
		$headerLogo.attr('href', "/tng/covid19/vaccine-site-finder.html");
		$headerLogo.find("img").attr('src', "/content/dam/covid19/anthem-logo.svg");
	}
    }

});