package com.anthem.vaccine.sites.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Anthem External API Configuration", description = "Configure the CloudMedx API Configuration Details")
public @interface AnthemExternalApiConfiguration {

	@AttributeDefinition(name = "CloudMedx Host Address", description = "Host Address for CloudMedx API")
	String cloudMedxHostAddress() default "https://c19explorer.io/v2.0";

	@AttributeDefinition(name = "CloudMedx Login API Endpoint", description = "CloudMedx Login Endpoint which would authenticate the user")
	String cloudMedxLoginEndpoint() default "/Admin/login";

	@AttributeDefinition(name = "CloudMedx Login API UserName", description = "Username for the CloudMedx Login API")
	String cloudMedxLoginUsername() default "c19_anthem_sydney_care";

	@AttributeDefinition(name = "CloudMedx Login API Password", description = "Password for the CloudMedx Login API")
	String cloudMedxLoginPassword() default "AnthEm*371";

	@AttributeDefinition(name = "CloudMedx Token Decoder Phrase", description = "CloudMedx Token Decoder Phrase")
	String cloudMedxTokenPhrase() default "cmx@540!";

	@AttributeDefinition(name = "CloudMedx Vaccine Trends/Doses API Endpoint", description = "CloudMedx Vaccine Trends/Doses API Endpoint")
	String cloudMedxVaccineTrendsEndpoint() default "/client/vaccine/cdc";

	@AttributeDefinition(name = "CloudMedx Vaccine Site Finder API Endpoint", description = "CloudMedx Vaccine Site Finder API Endpoint")
	String cloudMedxVaccineSitesEndpoint() default "/client/vaccine/vaccine_site";

	@AttributeDefinition(name = "CloudMedx Vaccine Eligibility Calculator API Endpoint", description = "CloudMedx Vaccine Eligibility Calculator API Endpoint")
	String cloudMedxEligibilityCalculatorEndpoint() default "/client/vaccine/LinePosition";

}