package com.anthem.vaccine.sites.core.config.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anthem.vaccine.sites.core.config.AnthemExternalApiConfiguration;
import com.anthem.vaccine.sites.core.config.AnthemExternalApiConfigurationService;

@Component(immediate = true, service = AnthemExternalApiConfigurationService.class)
@Designate(ocd = AnthemExternalApiConfiguration.class)
public class AnthemExternalApiConfigurationServiceImpl implements AnthemExternalApiConfigurationService {
	
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(AnthemExternalApiConfigurationServiceImpl.class);

	private AnthemExternalApiConfiguration config;

	private String cloudMedxHostAddress = "";

	@Activate
	public void activate(AnthemExternalApiConfiguration config) {

		LOG.trace("[Acivate method] AnthemExternalApiConfigurationServiceImpl ");
		this.config = config;
		String hostAddress = config.cloudMedxHostAddress();
		this.cloudMedxHostAddress = hostAddress.charAt(hostAddress.length() - 1) == '/'
				? hostAddress.substring(0, hostAddress.length() - 1)
				: hostAddress;

	}

	@Override
	public String getCloudMedxLoginUrl() {
		return verifyAndFormUrl(config.cloudMedxLoginEndpoint());
	}

	@Override
	public String getCloudMedxLoginUserName() {
		return config.cloudMedxLoginUsername();
	}

	@Override
	public String getCloudMedxLoginPassword() {
		return config.cloudMedxLoginPassword();
	}

	@Override
	public String getCloudMedxDecodeTokenPhrase() {
		return config.cloudMedxTokenPhrase();
	}

	@Override
	public String getCloudMedxVaccineTrendsUrl() {
		return verifyAndFormUrl(config.cloudMedxVaccineTrendsEndpoint());
	}

	@Override
	public String getCloudMedxVaccineSitesFinderUrl() {
		return verifyAndFormUrl(config.cloudMedxVaccineSitesEndpoint());
	}

	@Override
	public String getCloudMedxEligibilityCalculatorUrl() {
		return verifyAndFormUrl(config.cloudMedxEligibilityCalculatorEndpoint());
	}

	private String verifyAndFormUrl(String endpoint) {
		return endpoint.charAt(0) == '/' ? cloudMedxHostAddress.concat(endpoint)
				: cloudMedxHostAddress.concat("/").concat(endpoint);
	}

}