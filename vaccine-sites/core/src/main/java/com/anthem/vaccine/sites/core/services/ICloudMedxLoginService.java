package com.anthem.vaccine.sites.core.services;

import java.io.IOException;

public interface ICloudMedxLoginService {

	String fetchAuthToken() throws IOException;

}