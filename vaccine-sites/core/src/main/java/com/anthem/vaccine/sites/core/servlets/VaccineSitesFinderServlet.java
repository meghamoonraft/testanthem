package com.anthem.vaccine.sites.core.servlets;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.stream.Collectors;

import javax.servlet.Servlet;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.SlingHttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anthem.vaccine.sites.core.config.AnthemExternalApiConfigurationService;
import com.anthem.vaccine.sites.core.services.ICloudMedxLoginService;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component(service = Servlet.class, immediate = true, property = { 
		"sling.servlet.resourceTypes=" + "anthem/vaccine-site-finder",
		"sling.servlet.methods=" + HttpConstants.METHOD_POST })
public class VaccineSitesFinderServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(VaccineSitesFinderServlet.class);

	@Reference
	private transient ICloudMedxLoginService loginService;

	@Reference
	private transient AnthemExternalApiConfigurationService apiConfig;

	public static final String CONTENT_TYPE = "Content-Type";

	public static final String X_API_KEY = "X-API-KEY";

	public static final String APPLICATION_JSON = "application/json";

	public static final String UTF_8 = "UTF-8";

	@Override
	protected void doPost(SlingHttpServletRequest httpRequest, SlingHttpServletResponse response) throws IOException {

		String vaccineSitesUrl = apiConfig.getCloudMedxVaccineSitesFinderUrl();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String requestPayload = httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			LOG.debug("Request Payload {}", requestPayload);
			JSONObject jsonPayload = new JSONObject(requestPayload);
			ObjectMapper mapper = new ObjectMapper();
			RequestPayload requestBody = new RequestPayload();
			String state = jsonPayload.getString("state");
			if (null != state && !state.isEmpty()) {
				requestBody.setState(state);
			} else {
				requestBody.setState("");
			}
			String county = jsonPayload.getString("county");
			if (null != county && !county.isEmpty()) {
				requestBody.setCounty(county);
			} else {
				requestBody.setCounty("");
			}
			String latitude = jsonPayload.getString("latitude");
			if (null != latitude && !latitude.isEmpty()) {
				requestBody.setLat(latitude);
			} else {
				requestBody.setLat("");
			}
			String longitude = jsonPayload.getString("longitude");
			if (null != longitude && !longitude.isEmpty()) {
				requestBody.setLon(longitude);
			} else {
				requestBody.setLon("");
			}
			int distanceThreshold = jsonPayload.getInt("distanceThreshold");
			requestBody.setThreshold(distanceThreshold);

			String authenticationToken = loginService.fetchAuthToken();
			LOG.debug("Vaccine Site Finder URL : " + vaccineSitesUrl);
			String requestData = mapper.writeValueAsString(requestBody);
			LOG.debug("Request : " + requestData);

			HttpPost httpPostRequest = new HttpPost(vaccineSitesUrl);
			httpPostRequest.setEntity(new StringEntity(requestData));
			httpPostRequest.setHeader(CONTENT_TYPE, URLDecoder.decode(APPLICATION_JSON, UTF_8));
			httpPostRequest.setHeader(X_API_KEY, URLDecoder.decode(authenticationToken, UTF_8));

			CloseableHttpResponse httpResponse = httpclient.execute(httpPostRequest);
			StatusLine statusLine = httpResponse.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			LOG.debug("Completed execution of http request, received response code: " + statusCode);
			HttpEntity entity = httpResponse.getEntity();
			if (statusCode >= HttpStatus.SC_OK && statusCode < 300) {
				JSONObject responseObj = new JSONObject(EntityUtils.toString(entity));
				String encodedData = responseObj.getString("data");
				LOG.debug("Resposne data -- > " + encodedData);
				String[] split_string = encodedData.split("\\.");
				// String base64EncodedHeader = split_string[0];
				String base64EncodedBody = split_string[1];
				// String base64EncodedSignature = split_string[2];
				Base64 base64Url = new Base64(true);
				String responseString = new String(base64Url.decode(base64EncodedBody));
				LOG.debug(responseString);
				response.getWriter().write(responseString);
				response.setContentType("application/json");
			} else if (statusCode == 400) {
				response.setStatus(400);
				response.getWriter().write("Invalid Argument : Inappropriate argument value (of correct type) ");
			} else if (statusCode == 500) {
				response.setStatus(400);
				response.getWriter().write("Invalid Statecode !");
			}

		} catch (JSONException e) {
			LOG.error("JSON Exception while parsing the payload {}",e);
			response.setStatus(400);
			response.getWriter().write("Bad Request !");
		} catch (IOException e) {
            LOG.error("Exception captured in Vaccine Site finder servlet {}",e);
			response.setStatus(500);
			response.getWriter().write("Something went wrong from our end !");
		} finally {
			httpclient.close();
		}
	}

	public class RequestPayload {
		@JsonProperty("state")
		private String state;

		@JsonProperty("county")
		private String county;

		@JsonProperty("lat")
		private String lat;

		@JsonProperty("lon")
		private String lon;

		@JsonProperty("threshold")
		private Integer threshold;

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getCounty() {
			return county;
		}

		public void setCounty(String county) {
			this.county = county;
		}

		public String getLat() {
			return lat;
		}

		public void setLat(String lat) {
			this.lat = lat;
		}

		public String getLon() {
			return lon;
		}

		public void setLon(String lon) {
			this.lon = lon;
		}

		public Integer getThreshold() {
			return threshold;
		}

		public void setThreshold(Integer threshold) {
			this.threshold = threshold;
		}

	}

}