package com.anthem.vaccine.sites.core.services.impl;

import java.io.IOException;
import java.net.URLDecoder;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anthem.vaccine.sites.core.config.AnthemExternalApiConfigurationService;
import com.anthem.vaccine.sites.core.services.ICloudMedxLoginService;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component(service = ICloudMedxLoginService.class, immediate = true)
public class CloudMedxLoginServiceImpl implements ICloudMedxLoginService {
	
	private static final long serialVersionUID = 1L;;

	private static final Logger LOG = LoggerFactory.getLogger(CloudMedxLoginServiceImpl.class);

	@Reference
	private AnthemExternalApiConfigurationService apiConfig;

	@Override
	public String fetchAuthToken() throws IOException {

		String loginUrl = apiConfig.getCloudMedxLoginUrl();
		String userName = apiConfig.getCloudMedxLoginUserName();
		String password = apiConfig.getCloudMedxLoginPassword();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		LOG.debug("Inside fetchAuthToken Method , Obtained configurations as {}", loginUrl, " | ", userName, " | ",
				password);
		try {
			ObjectMapper mapper = new ObjectMapper();
			LoginRequestDto request = new LoginRequestDto();
			request.setUsername(userName);
			request.setPassword(password);

			String requestData = mapper.writeValueAsString(request);
			LOG.debug("Request Data : " + requestData);

			HttpPost httpRequest = new HttpPost(loginUrl);
			httpRequest.setEntity(new StringEntity(requestData));
			httpRequest.setHeader("Content-Type", URLDecoder.decode("application/json", "UTF-8"));

			CloseableHttpResponse response = httpclient.execute(httpRequest);

			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			LOG.debug("Completed execution of http request, received response code: " + statusCode);
			HttpEntity entity = response.getEntity();
			JSONObject responseObj = new JSONObject(EntityUtils.toString(entity));
			if (statusCode >= HttpStatus.SC_OK && statusCode < 300) {
				return responseObj.getString("token");
			} else {
				return responseObj.getString("message");
			}
		} catch (JSONException e) {
			LOG.error("An JSON Parsing error occurred while invoking an endpoint.", e);
			return e.getMessage();			
		} 
		catch (IOException e) {
			LOG.error("An error occurred while invoking an endpoint.", e);
			return e.getMessage();
		}
		finally {
			httpclient.close();
		}
	}

	public class LoginRequestDto {

		@JsonProperty("username")
		private String username;

		@JsonProperty("password")
		private String password;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

}
