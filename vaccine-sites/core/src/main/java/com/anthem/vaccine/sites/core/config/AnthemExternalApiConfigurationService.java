package com.anthem.vaccine.sites.core.config;

public interface AnthemExternalApiConfigurationService {

	String getCloudMedxLoginUrl();

	String getCloudMedxLoginUserName();

	String getCloudMedxLoginPassword();

	String getCloudMedxDecodeTokenPhrase();

	String getCloudMedxVaccineTrendsUrl();

	String getCloudMedxVaccineSitesFinderUrl();

	String getCloudMedxEligibilityCalculatorUrl();

}