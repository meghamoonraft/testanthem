package com.anthem.hrportal.core.models;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;

@Model(
        adaptables = {SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class UserLogin {

    @Self
    SlingHttpServletRequest slingHttpServletRequest;

    private String loggedIn = "false";
    private String userId;

    @PostConstruct
    public void init(){
            ResourceResolver resourceResolver = slingHttpServletRequest.getResourceResolver();
            if (!resourceResolver.getUserID().equals("anonymous")) {
                userId = resourceResolver.getUserID();
                this.loggedIn = "true";
            }
        }

    public String getLoggedIn() {
        return loggedIn;
    }
    public String getUserId(){
        return userId;
    }
}
