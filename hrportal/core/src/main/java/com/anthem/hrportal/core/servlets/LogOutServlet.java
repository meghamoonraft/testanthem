package com.anthem.hrportal.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;


@Component(service = Servlet.class, immediate = true, property = { 
		"sling.servlet.resourceTypes=" + "anthem/logout",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET })
public class LogOutServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = -4724004568324021937L;
	
	public static final String cookieName ="login-token";

	@Override
	protected void doGet(SlingHttpServletRequest httpRequest, SlingHttpServletResponse response) throws IOException {
		Cookie[] cookies = httpRequest.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if (cookieName.equals(cookie.getName())) {
					StringBuilder cookieString = new StringBuilder(cookieName + "=" + "" + "; ");
					cookieString.append("Expires=0" + "; ");
					cookieString.append("Version=0; ");
					cookieString.append("Path=/; ");
					cookieString.append("Max-Age=0" + "; ");
					response.addHeader("Set-Cookie", cookieString.toString());
				}
			}
		}
	}
}
