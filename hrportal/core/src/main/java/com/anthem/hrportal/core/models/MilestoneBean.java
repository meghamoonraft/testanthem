package com.anthem.hrportal.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MilestoneBean {

	@ValueMapValue
	private String milestoneTitle;

	@ValueMapValue
	private String milestoneSubheading;

	@ValueMapValue
	private String milestoneImage;

	@ValueMapValue
	private String milestoneLabel;

	public String getMilestoneTitle() {
		return milestoneTitle;
	}

	public String getMilestoneSubheading() {
		return milestoneSubheading;
	}

	public String getMilestoneImage() {
		return milestoneImage;
	}

	public String getMilestoneLabel() {
		String label = (milestoneLabel != null) ? milestoneLabel : "Milestone";
		return label;
	}

}
