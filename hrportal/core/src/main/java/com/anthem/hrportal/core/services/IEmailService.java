package com.anthem.hrportal.core.services;

public interface IEmailService {

	boolean sendEmail(String mailSubject, String body, String to, String cc, String bcc);

}
