package com.anthem.hrportal.core.models;

import java.util.Date;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CtaSimpleCardBean {
	
	@ValueMapValue
	private String ctaHeading;
	
	@ValueMapValue
	private String cardImage;
	
	@ValueMapValue
	private String ctaImageAlt;
	
	@ValueMapValue
	private String ctaDescription;
	
	@ValueMapValue
	private String ctaLink;
	
	@ValueMapValue
	private String ctaLabel;
	

	public String getCtaHeading() {
		return ctaHeading;
	}

	public String getCardImage() {
		return cardImage;
	}

	public String getCtaImageAlt() {
		return ctaImageAlt;
		}

	public String getCtaDescription() {
		return ctaDescription;
	}	

	public String getCtaLink() {
		return ctaLink;
	}

	public String getCtaLabel() {
		return ctaLabel;
	}



	
}
