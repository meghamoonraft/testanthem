package com.anthem.hrportal.core.models;

import java.util.Collections;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CtaCardModel {
	@ValueMapValue
	private String selectVariation;

	@ChildResource(name="simpleCard")
	private List<CtaSimpleCardBean> simpleCard;
	
	@ChildResource(name="linksCard")
	private List<CtaSimpleCardBean> linksCard;

	public String getSelectVariation() {
		return selectVariation;
	}

	public List<CtaSimpleCardBean> getSimpleCard() {
		return Collections.unmodifiableList(simpleCard);
	}

	public List<CtaSimpleCardBean> getLinksCard() {
		return Collections.unmodifiableList(linksCard);
	}

	
		
}
