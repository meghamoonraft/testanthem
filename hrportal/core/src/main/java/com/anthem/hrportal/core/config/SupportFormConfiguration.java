package com.anthem.hrportal.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Gateway to Anthem Support Form Configuration", description = "Configure the Email Template Details")
public @interface SupportFormConfiguration {
	
	@AttributeDefinition(name = "Email To Address", description = "Specify DL details or To Address", type = AttributeType.STRING)
	String emailToAddress() default "IntegrationQuestions@anthem.com";
	
	@AttributeDefinition(name = "Email Subject", description = "Specify Subject for the mail", type = AttributeType.STRING)
	String emailSubject() default "REG : GatewayToAnthem [Support]";
	
	@AttributeDefinition(name = "Email Body Template", description = "Specify Email Template", type = AttributeType.STRING)
	String emailBodyTemplate() default "";

}
