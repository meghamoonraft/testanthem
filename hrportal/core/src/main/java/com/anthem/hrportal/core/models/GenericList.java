package com.anthem.hrportal.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public interface GenericList {

	@ChildResource(name = "genericListItems")
    List<ListItem> getGenericListItems();

    @Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    interface ListItem{

        @ValueMapValue
        String getItemTitle();

        @ValueMapValue
        String getSubtitle();

        @ValueMapValue
        String getItemLink();

        @ValueMapValue
        String getItemlinkTarget();
        
        @ValueMapValue
        String getExternalLink();
        
    }


}
