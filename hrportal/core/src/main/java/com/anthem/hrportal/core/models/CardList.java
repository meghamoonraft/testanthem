package com.anthem.hrportal.core.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.annotation.PostConstruct;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.joda.time.DateTime;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Style;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CardList {
	@ScriptVariable
	private ValueMap properties;
	
	@SlingObject
	private SlingHttpServletRequest request;

	private static final  String EVENT_RES_TYPE="anthemhr/hrportal/components/eventdetails";
	private static final  String ARTICLE_RES_TYPE="anthemhr/hrportal/components/article-title";
	private static final String IMAGE_RES_TYPE="anthemhr/hrportal/components/image";
	private static final String CALENDAR_RES_TYPE="anthemhr/hrportal/components/calendar";
	private static final String BRIGHTCOVE_RES_TYPE="anthemhr/hrportal/components/brightcove-player";
	private static final  String BRIGHTCOVE_ASSET_PATH="/content/dam/brightcove_videos/";
	private static final String BRIGHTCOVE_THUMBNAIL_PATH="/jcr:content/renditions/brc_poster.png";
	private static final String PN_TAGS="cq:tags";
	List<Card> cardlist = new ArrayList<Card>();
	String PARENT_PAGE = "parentPage";
	String TAG_PARENT_PAGE="tagsSearchRoot";
	String SOURCE = "listFrom";
	String ORDER_BY = "orderBy";
	String image=null;
	String alt=null;
	String video=null;
	String json=null;
	
	@ScriptVariable
	private Style currentStyle;

	String TAGS = "tags";
	String CHILDREN="children";
	
	@ScriptVariable
	private Page currentPage;
	String SORT_ORDER = "sortOrder";
	private static final String CHILD_DEPTH = "childDepth";
	private static final int CHILD_DEPTH_DEFAULT = 1;
	
	@SlingObject
	private ResourceResolver resourceResolver;

	private PageManager pageManager;

	public Source getListType() {		
		return Optional.ofNullable(				
				Optional.ofNullable(properties.get(SOURCE, String.class))						
						.orElseGet(() -> Optional.ofNullable(currentStyle.get(SOURCE, String.class)).orElse(null)))
						.map(Source::fromString)				
						.orElse(Source.EMPTY);
	}

	public java.util.List<Page> getPages() {
		
		Stream<Page> itemStream;
		switch (getListType()) {
		case STATIC:
			itemStream = getStaticListItems();
			break;		
		case CHILDREN:
			itemStream = getChildListItems();
			break;
		case TAGS:
			itemStream = getChildListItems();
			break;
		default:
			itemStream = Stream.empty();
			break;
		}

		// order the results
		OrderBy orderBy = OrderBy.fromString(properties.get(ORDER_BY, StringUtils.EMPTY));

		if (orderBy != null) {
			SortOrder sortOrder = SortOrder.fromString(properties.get(SORT_ORDER, SortOrder.ASC.value));
			itemStream = itemStream.sorted(new ListSort(orderBy, sortOrder));
		}
		// collect the results
		return itemStream.collect(Collectors.toList());
	}
	public Stream<Page> getStaticListItems() {
		Stream<Page> getStaticListItems = Arrays.stream(this.properties.get("pages", new String[0]))
				.map(pageManager::getContainingPage).filter(Objects::nonNull);
		return getStaticListItems;
	}
	
	public Stream<Page> getChildListItems() {

		int childDepth = properties.get(CHILD_DEPTH, CHILD_DEPTH_DEFAULT);
		Optional<Page> rootPage1 = getRootPage(PARENT_PAGE);
		if(properties.get(SOURCE).equals(CHILDREN)) {
		 rootPage1 = getRootPage(PARENT_PAGE);
		}
		else if(properties.get(SOURCE).equals(TAGS)) {
			rootPage1 = getRootPage(TAG_PARENT_PAGE);
		}
		return rootPage1.map(rootPage -> collectChildren(childDepth, rootPage)).orElseGet(Stream::empty);
	}
	
	public static Stream<Page> collectChildren(int depth, final Page parent) {

		if (depth <= 0) {
			return Stream.empty();
		}
		Iterator<Page> childIterator = parent.listChildren();
		return StreamSupport.stream(((Iterable<Page>) () -> childIterator).spliterator(), false)
				.flatMap(child -> Stream.concat(Stream.of(child), collectChildren(depth - 1, child)));
	} 
	
	@PostConstruct
	public void postConstruct() {
		pageManager = resourceResolver.adaptTo(PageManager.class);
		List<Page> listItems = getPages();		
		String requestPath = request.getRequestPathInfo().getResourcePath();
		Resource requestResource =resourceResolver.getResource(requestPath);
		listItems.forEach(item -> {
			Resource contentResource = item.getContentResource();
			getChildResource(contentResource, item.getPath(),item.getTitle());
		});		
		if(requestResource.getResourceType().equalsIgnoreCase(CALENDAR_RES_TYPE)) {
		json=prepareFinalJson(cardlist);
		}
	}
	
	public void getChildResource(Resource contentResource, String path,String title) {
		Iterator<Resource> childResourceIterator = contentResource.listChildren();
		while (null != childResourceIterator && childResourceIterator.hasNext()) {
			Resource childResource = childResourceIterator.next();
			if (childResource.getResourceType().equals(IMAGE_RES_TYPE)) {
				video=null;
				image=childResource.getValueMap().get("fileReference", String.class);
				alt=childResource.getValueMap().get("alt", String.class);					
				continue;
			}
			else if (childResource.getResourceType().equals(BRIGHTCOVE_RES_TYPE)) {
				image=null;
				String account=childResource.getValueMap().get("account", String.class);
				String videoPlayerPL=childResource.getValueMap().get("videoPlayer", String.class);
				video=BRIGHTCOVE_ASSET_PATH.concat(account).concat("/").concat(videoPlayerPL).concat(".mp4")
						.concat(BRIGHTCOVE_THUMBNAIL_PATH);
				continue;
			}		
			else if (childResource.getResourceType().equals(ARTICLE_RES_TYPE) || childResource.getResourceType().equals(EVENT_RES_TYPE)  ) {
					String tag  = childResource.getValueMap().get(PN_TAGS, String.class);
			if(TAGS.equals(properties.get(SOURCE, String.class)) &&  properties.get(TAGS, String.class).equalsIgnoreCase(tag)) {
				Card card = childResource.adaptTo(Card.class);
				if(image!=null) {
					card.setImage(image);
					card.setAlt(alt);
					}
					if(video!=null) {card.setVideo(video);}
				if(title !=null) {card.setPageTitle(title);}
				if (path != null) {
					card.setPath(path);
				}				
				cardlist.add(card);
				break;				
			}
			else if(!TAGS.equals(properties.get(SOURCE, String.class)))  {
				Card card = childResource.adaptTo(Card.class);
				if(image!=null) {
				card.setImage(image);
				card.setAlt(alt);
				}
				if(video!=null) {card.setVideo(video);}
				if(title !=null) {card.setPageTitle(title);}
				if (path != null) {
					card.setPath(path);
				}				
				cardlist.add(card);
				break;
			}
			}			
			else {
				if (childResource.hasChildren())			
				getChildResource(childResource, path,title);
			}
			
		}
	}

	
	public List<Card> getCardlist() {		
		return cardlist ;
	}	

	public String getJson() {
		return json;
	}

	public Optional<Page> getRootPage(final String fieldName) {
		Optional<String> parentPath = Optional.ofNullable(this.properties.get(fieldName, String.class))
				.filter(StringUtils::isNotBlank);

		// no path is specified, use current page
		if (!parentPath.isPresent()) {
			return Optional.of(this.currentPage);
		}

		// a path is specified, get that page or return null
		return parentPath.map(resourceResolver::getResource).map(currentPage.getPageManager()::getContainingPage);
	}

	public String prepareFinalJson(List<Card> cardlist2) {
		 Gson gson = new GsonBuilder()
				 .setDateFormat("E MMM dd HH:mm:ss z yyyy")
				 .create();
		 Map<String, List<Card>> json = new HashMap<>();
		 json.put("result", cardlist2);
		 return gson.toJson(json);
	}

	public enum Source {
		CHILDREN("children"), STATIC("static"), TAGS("tags"), EMPTY(StringUtils.EMPTY);

		private final String value;

		Source(String value) {
			this.value = value;
		}
		public static Source fromString(String value) {
			for (Source s : values()) {
				if (StringUtils.equals(value, s.value)) {
					return s;
				}
			}
			return null;
		}
	}
	public enum SortOrder {		
		ASC("asc"),		
		DESC("desc");
		private final String value;
		SortOrder(String value) {
			this.value = value;
		}
		public static SortOrder fromString(String value) {
			for (SortOrder s : values()) {
				if (StringUtils.equals(value, s.value)) {
					return s;
				}
			}
			return ASC;
		}
	}

	public enum OrderBy {		
		DATE("date");
		private final String value;
		OrderBy(String value) {
			this.value = value;
		}		
		public static OrderBy fromString(String value) {
			for (OrderBy s : values()) {
				if (StringUtils.equals(value, s.value)) {
					return s;
				}
			}
			return null;
		}
	}

	private class ListSort implements Comparator<Page>, Serializable {

		private static final long serialVersionUID = 1171225002657014881L;
		private final SortOrder sortOrder;
		private final OrderBy orderBy;
		ListSort(final OrderBy orderBy, final SortOrder sortOrder) {
			this.orderBy = orderBy;
			this.sortOrder = sortOrder;
		}

		public int compare(final Page item1, final Page item2) {
			int i = 0;			
			if (orderBy == OrderBy.DATE) {	
				Calendar date=null;
				Calendar date1=getComponentdate(item1.getContentResource(), date);
				Calendar date2=getComponentdate(item2.getContentResource(), date);
				i = ObjectUtils.compare(date1, date2, false);
			} 			
			if (sortOrder == SortOrder.DESC) {
				i = i * -1;
			}
			return i;
		}			
	}
	public Calendar getComponentdate(Resource item, Calendar date) {
		Iterator<Resource> childResource = item.listChildren();
		while (null != childResource && childResource.hasNext()) {
			Resource child = childResource.next();
			if (child.getResourceType().equals(ARTICLE_RES_TYPE)) {
				ValueMap valueMap = child.getValueMap();
				date=valueMap.get("publishDate", Calendar.class);
				break;
			}
		 if(child.getResourceType().equals(EVENT_RES_TYPE)) {	
			 ValueMap valueMap = child.getValueMap();
			 date=getDateAndTime(valueMap.get("startDate", Calendar.class),valueMap.get("startTime", Calendar.class));
			 break;
			} 
			if (child.hasChildren())
					date=getComponentdate(child,date);						
		}
		return date;
	}

	public Calendar getDateAndTime(Calendar date, Calendar time) {
		DateTime dt=new DateTime(date);
		 DateTime ti=new DateTime(time);
		 int year=dt.getYear();
		 int month=dt.getMonthOfYear();
		 int day=dt.getDayOfMonth();
		 int hour=ti.getHourOfDay();
		 int minute=ti.getMinuteOfHour();
		 date.set(year, month, day, hour, minute,0);
		return date ;
	}
}