package com.anthem.hrportal.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContactUsBeanModel {

	
	@ValueMapValue
	private String authorImage;
	
	@ValueMapValue
	private String authorDesig;
	
	@ValueMapValue
	private String authorName;
		
	@ValueMapValue
	private String linkedinIcon;
	
	@ValueMapValue
	private String linkedinAlt;
	
	@ValueMapValue
	private String linkedinLink;
	
	@ValueMapValue
	private String letsConnect;

	public String getAuthorImage() {
		return authorImage;
	}

	public String getAuthorDesig() {
		return authorDesig;
	}

	public String getAuthorName() {
		return authorName;
	}

	public String getLinkedinIcon() {
		return linkedinIcon;
	}

	public String getLinkedinAlt() {
		return linkedinAlt;
	}

	public String getLinkedinLink() {
		return linkedinLink;
	}

	public String getLetsConnect() {
		return letsConnect;
	}
	
	
	
}
