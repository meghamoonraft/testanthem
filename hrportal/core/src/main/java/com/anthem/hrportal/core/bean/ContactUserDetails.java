package com.anthem.hrportal.core.bean;

public class ContactUserDetails {

	private String fullName;

	private String emailId;

	private String contactNumber;

	private String message;

	public String getFullName() {
		return fullName;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}