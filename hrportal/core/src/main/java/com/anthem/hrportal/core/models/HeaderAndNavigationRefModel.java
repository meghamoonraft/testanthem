package com.anthem.hrportal.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.wcm.api.Page;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class })
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class HeaderAndNavigationRefModel {
	
	private static final String DEFAULT_GATEWAY_TO_ANTHEM_PATH = "/content/gatewaytoanthem/";
	private static final String DEFAULT_SITE_NAME = "beacon";
	private static final String DEFAULT_LANGUAGE_ROOT="en";
	private static final String SEPERATOR ="/";

	private static final String DEFAULT_HEADER_PATH = "/content/gatewaytoanthem/beacon/en/admin-console/jcr:content/root/container/header";
	private static final String DEFAULT_HEADER_SRC_PATH_FROM_ADMIN_PAGE = "/admin-console/jcr:content/root/container/header";

	private static final String DEFAULT_NAVIGATION_PATH = "/content/gatewaytoanthem/beacon/en/admin-console/jcr:content/root/container/navigation";
	private static final String DEFAULT_NAVIGATION_SRC_PATH_FROM_ADMIN_PAGE = "/admin-console/jcr:content/root/container/navigation";

	private String headerRefPath;

	private String navigationRefPath;


	@javax.annotation.Resource
	private Resource resource;

	@Inject
	private Page resourcePage;

	@PostConstruct
	protected void init() {
		
		String pagePath;
		
		if (null != resourcePage) {
			
			pagePath = resourcePage.getPath();
			
			String LANGUAGE_ROOT = extractSpecificKeyFromPath(pagePath,4,DEFAULT_LANGUAGE_ROOT);

			headerRefPath = generateComponentPathUsingPagePath(DEFAULT_HEADER_PATH,pagePath,LANGUAGE_ROOT,DEFAULT_HEADER_SRC_PATH_FROM_ADMIN_PAGE);
			
			navigationRefPath = generateComponentPathUsingPagePath(DEFAULT_NAVIGATION_PATH,pagePath,LANGUAGE_ROOT,DEFAULT_NAVIGATION_SRC_PATH_FROM_ADMIN_PAGE);
		}
	}

	public String getHeaderRefPath() {
		return headerRefPath;
	}

	public String getNavigationRefPath() {
		return navigationRefPath;
	}
	
	private String generateComponentPathUsingPagePath(String defaultComponentPath,String currentPagePath,String languageRoot ,String defaultComponentTailPath) {
		if (StringUtils.isNotBlank(currentPagePath)) {
			String siteName = extractSpecificKeyFromPath(currentPagePath,3,DEFAULT_SITE_NAME);
			return DEFAULT_GATEWAY_TO_ANTHEM_PATH + siteName +SEPERATOR+languageRoot+ defaultComponentTailPath;
		}
		return defaultComponentPath;
	}
	
	private String extractSpecificKeyFromPath(String pagePath, Integer pos, String defaultValue) {
		String[] arr = pagePath.split("/");
		if (arr.length > pos) {
			return arr[pos];
		}
		return defaultValue;
	}
}