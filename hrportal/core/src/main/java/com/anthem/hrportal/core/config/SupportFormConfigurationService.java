package com.anthem.hrportal.core.config;

public interface SupportFormConfigurationService {

	String getEmailToAddress();

	String getEmailSubject();

	String getEmailBodyTemplate();

}
