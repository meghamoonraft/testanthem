package com.anthem.hrportal.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class QuickLinksBean {
	@ValueMapValue
	private String quickLinkTitle;

	@ValueMapValue
	private String quickLinkImage;
	
	@ValueMapValue
	private String buttonCta;
	
	@ValueMapValue
	private String externalLink;
	
	@ValueMapValue
	private String linkTarget;

	public String getQuickLinkTitle() {
		return quickLinkTitle;
	} 

	public String getQuickLinkImage() {
		return quickLinkImage;
	}

	public String getButtonCta() {
		return buttonCta;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public String getLinkTarget() {
		return linkTarget;
	}

	
}
