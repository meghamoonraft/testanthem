package com.anthem.hrportal.core.models;

import java.util.Collections;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;


@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderModel {
	
@ChildResource(name = "languageDropDown")
private List<HeaderBeanModel> languageDropDown;

public List<HeaderBeanModel> getLanguageDropDown() {
	return Collections.unmodifiableList(languageDropDown);
}


}
