package com.anthem.hrportal.core.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.sling.xss.XSSAPI;
import javax.servlet.Servlet;

import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anthem.hrportal.core.bean.ContactUserDetails;
import com.anthem.hrportal.core.config.SupportFormConfigurationService;
import com.anthem.hrportal.core.services.IEmailService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;

@Component(service = Servlet.class, immediate = true, property = { "sling.servlet.resourceTypes=" + "anthem/contactus",
		"sling.servlet.methods=" + HttpConstants.METHOD_POST })
public class ContactUsEmailServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(ContactUsEmailServlet.class);
	private static final List<String> mapKeys = Arrays.asList("fullName", "emailId", "contactNumber","message");
	public static final String EMPTY_JSON_OBJECT = "{}";

	@Reference
	private transient IEmailService emailService;

	@Reference
	private transient SupportFormConfigurationService emailConfig;
	
	@Reference
	private transient XSSAPI xssAPI;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		try {
			LOG.trace("Inside ContactUs Servlet");
			HashMap<String, String> payload = fetchRequestPayload(request);
			if (null != payload) {
				String messageBody = replaceToken(payload, emailConfig.getEmailBodyTemplate());
				boolean isSuccess = emailService.sendEmail(emailConfig.getEmailSubject(), messageBody,
						emailConfig.getEmailToAddress(), "", "");
				if(isSuccess) {
					setResponse(response, "Mail Sent Successfully !", HttpStatus.SC_OK);
				} else {
					setResponse(response, "Service Unavailable !", HttpStatus.SC_SERVICE_UNAVAILABLE );
				}
			} else {
				setResponse(response, "Bad Request !", HttpStatus.SC_BAD_REQUEST);
			}
		} catch (IOException e) {
			setResponse(response, "Internal Server Error !", HttpStatus.SC_INTERNAL_SERVER_ERROR);
		}
	}

	private void setResponse(SlingHttpServletResponse response, String message, Integer statusCode) throws IOException {
		response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");
		response.setStatus(statusCode);
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode jsonNode = mapper.createObjectNode();
		if (statusCode.equals(HttpStatus.SC_OK)) {
			jsonNode.put("success", true);
		} else {
			jsonNode.put("success", false);
		}
		jsonNode.put("message", message);
		response.getWriter().write(jsonNode.toString());
	}
	
	private String replaceToken(HashMap<String, String> tokenMap, String text) {
		StringBuffer result = new StringBuffer();
		Pattern p = Pattern.compile("\\$\\{([\\w\\.]+)\\}");
		Matcher m = p.matcher(text);
		while (m.find() != false) {
			String token = m.group().replaceAll("[${}]", "");
			if (null != tokenMap.get(token)) {
				m.appendReplacement(result, tokenMap.get(token));
			} else {
				m.appendReplacement(result, "");
			}
		}
		m.appendTail(result);
		return result.toString();
	}

	private HashMap<String, String> fetchRequestPayload(SlingHttpServletRequest request) {
		try {
			ContactUserDetails userDetails = sanitizeInputStreamToObject(request.getInputStream(),ContactUserDetails.class);
			if(null!=userDetails) {
				String requestJsonStr = convertObjectToJson(userDetails,false);
				if (null != requestJsonStr) {
					ObjectMapper objectMapper = new ObjectMapper();
					HashMap<String, String> requestMap = objectMapper.readValue(requestJsonStr,
							new TypeReference<HashMap<String, String>>() {
							});
					if(!mapKeys.containsAll(requestMap.keySet())) {
						return null;
					}
					return requestMap;
				}
			}
		} catch (IOException e) {
			LOG.error("Error occured while extracting the payload : ", e);
			return null;
		}
		return null;
	}
	
	public <T> T sanitizeInputStreamToObject(InputStream stream, Class<T> clazz) {
		T obj = null;
		try {
			String inputString = IOUtils.toString(stream, StandardCharsets.UTF_8);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			obj = mapper.readValue(sanitizedJsonString(inputString), clazz);
		} catch (IOException e) {
			LOG.error("Error reading Sanitized String value ",e);
		}
		return obj;
	}
	
	public String sanitizedJsonString(String jsonStr) {
		return xssAPI.getValidJSON(jsonStr,EMPTY_JSON_OBJECT);
	}
	
	public <T> String convertObjectToJson(T model, Boolean includeNull) {
		String jsonString = EMPTY_JSON_OBJECT;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			if(!includeNull.booleanValue()) mapper.setSerializationInclusion(Include.NON_NULL);
			jsonString = mapper.writeValueAsString(model);
		} catch (IOException e) {
			LOG.error("Error parsing JSON ",e);
		} 
		return jsonString;
	}
}
