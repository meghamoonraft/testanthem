package com.anthem.hrportal.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderBeanModel {

	@ValueMapValue
	private String language;
	
	@ValueMapValue
	private String languageKey;

	public String getLanguage() {
		return language;
	}

	public String getLanguageKey() {
		return languageKey;
	}
		
}
