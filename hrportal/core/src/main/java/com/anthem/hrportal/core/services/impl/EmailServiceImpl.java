package com.anthem.hrportal.core.services.impl;

import com.anthem.hrportal.core.services.IEmailService;
import com.day.cq.mailer.MailingException;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = IEmailService.class, immediate = true)
public class EmailServiceImpl implements IEmailService, Serializable {

	private static final long serialVersionUID = 1518907472444849061L;
	private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

	public static final int DEFAULT_CONNECT_TIMEOUT = 30000;

	public static final int DEFAULT_SOCKET_TIMEOUT = 30000;

	private static final String PROP_SO_TIMEOUT = "so.timeout";

	private static final String PROP_CONNECT_TIMEOUT = "conn.timeout";

	private int connectTimeout;
	private int soTimeout;

	@Reference
	private transient MessageGatewayService messageGatewayService;

	@Activate
	protected void activate(Map<String, Object> config) {
		connectTimeout = PropertiesUtil.toInteger(config.get(PROP_CONNECT_TIMEOUT), DEFAULT_CONNECT_TIMEOUT);
		soTimeout = PropertiesUtil.toInteger(config.get(PROP_SO_TIMEOUT), DEFAULT_SOCKET_TIMEOUT);
	}

	public boolean sendEmail(String subject, String body, String to, String cc, String bcc) {
		try {
			LOG.trace("Mail Params :" + body + " to address :" + to + " bcc: " + bcc);

			final MessageGateway<Email> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
			HtmlEmail email = new HtmlEmail();
			if (connectTimeout > 0) {
				email.setSocketConnectionTimeout(connectTimeout);
			}
			if (soTimeout > 0) {
				email.setSocketTimeout(soTimeout);
			}
			if(null!=cc && !cc.isEmpty()) {
				email.addCc(cc);
			}
			if(null!=bcc && !bcc.isEmpty()) {
				email.addCc(bcc);
			}
			email.setSubject(subject);
			email.setHtmlMsg(body);
			email.setTo(Collections.singleton(new InternetAddress(to)));
			messageGateway.send(email);

			LOG.trace("Email sent successfully....");
		} catch (EmailException e) {
			LOG.error("Exception while sending Mail : " + e.getMessage() + " " + e);
			return false;
		} catch (AddressException e) {
			LOG.error("Exception while parsing To address  : " + e.getMessage() + " " + e);
			return false;
		} catch (MailingException e) {
			LOG.error("Conenction failed while initiating connection to SMTP Server  : " + e.getMessage() + " " + e);
			return false;
		}
		return true;
	}
}