package com.anthem.hrportal.core.config.impl;

import java.io.Serializable;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anthem.hrportal.core.config.SupportFormConfiguration;
import com.anthem.hrportal.core.config.SupportFormConfigurationService;

@Component(immediate = true, service = SupportFormConfigurationService.class)
@Designate(ocd = SupportFormConfiguration.class)
public class SupportFormConfigurationServiceImpl implements SupportFormConfigurationService,Serializable {
	
	private static final long serialVersionUID = 4272349526717765332L;
	private static final Logger LOG = LoggerFactory.getLogger(SupportFormConfigurationServiceImpl.class);
	
	private String emailToAddress;
	
	private String emailSubject;
	
	private String emailBodyTemplate;

	@Activate
	public void activate(SupportFormConfiguration supportFormConfiguration) {

		LOG.trace("[Acivate method] SupportFormConfigurationServiceImpl");
		this.emailToAddress =supportFormConfiguration.emailToAddress();
		this.emailSubject=supportFormConfiguration.emailSubject();
		this.emailBodyTemplate=supportFormConfiguration.emailBodyTemplate();
	} 

	@Override
	public String getEmailToAddress() {
		return emailToAddress;
	}

	@Override
	public String getEmailSubject() {
		return emailSubject;
	}

	@Override
	public String getEmailBodyTemplate() {
		return emailBodyTemplate;
	}

}
