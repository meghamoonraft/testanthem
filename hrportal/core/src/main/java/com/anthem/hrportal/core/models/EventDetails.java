package com.anthem.hrportal.core.models;

public class EventDetails {

	private String eventTitle;
	private String description;
	private String startDate;
	private String startTime;
	private String endDate;
	private String endTime;
	private String timezone;
	private String organiser;
	private String location;
	private String meetingLink;

	public String getEventTitle() {
		return eventTitle;
	}

	public String getDescription() {
		return description;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getTimezone() {
		return timezone;
	}

	public String getOrganiser() {
		return organiser;
	}

	public String getLocation() {
		return location;
	}

	public String getMeetingLink() {
		return meetingLink;
	}

	

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public void setOrganiser(String organiser) {
		this.organiser = organiser;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setMeetingLink(String meetingLink) {
		this.meetingLink = meetingLink;
	}
}
