package com.anthem.hrportal.core.models;


import java.util.Date;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class,
		SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Card {
	
	private static final String PN_TAGS="cq:tags";
	
	@ValueMapValue
	private Date publishDate;
	
	@ValueMapValue
	private String path;
	
	@ValueMapValue
	private String altText;
	
	@ValueMapValue
	private Date startDate;
	
	@ValueMapValue
	private Date startTime;
	
	@ValueMapValue
	private String eventImage;
	
	@ValueMapValue
	private String image;
	
	@ValueMapValue	
	private String video;
	
	@ValueMapValue
	private String pageTitle;
	
	@ValueMapValue(name=PN_TAGS)
	private String tag;
	
	@ValueMapValue
	private String tagIcon;
	
	@ValueMapValue
	private String tagBackground;	
	
	@ValueMapValue
	private String alt;	

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}


	public String getTagBackground() {
		return tagBackground;
	}


	public void setTagBackground(String tagBackground) {
		this.tagBackground = tagBackground;
	}


	public String getTag() {
		return tag;
	}


	public void setTag(String tag) {
		this.tag = tag;
	}


	public String getTagIcon() {
		return tagIcon;
	}


	public void setTagIcon(String tagIcon) {
		this.tagIcon = tagIcon;
	}


	public String getPageTitle() {
		return pageTitle;
	}


	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}


	public Date getPublishDate() {
		return (Date) publishDate.clone();
	}


	public void setPublishDate(Date publishDate) {
		this.publishDate = (Date) publishDate.clone();
	}


	public Date getStartDate() {
		return (Date) startDate.clone();
	}


	public void setStartDate(Date startDate) {
		this.startDate = (Date) startDate.clone();
	}


	public Date getStartTime() {
		return (Date) startTime.clone();
	}


	public void setStartTime(Date startTime) {
		this.startTime = (Date) startTime.clone();
	}



	public String getAltText() {
		return altText;
	}


	public void setAltText(String altText) {
		this.altText = altText;
	}


	public String getEventImage() {
		return eventImage;
	}


	public void setEventImage(String eventImage) {
		this.eventImage = eventImage;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getVideo() {
		return video;
	}


	public void setVideo(String video) {
		this.video = video;
	}


	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	
	

}

