package com.anthem.hrportal.core.servlets;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anthem.hrportal.core.models.EventDetails;

import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.Organizer;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.util.MapTimeZoneCache;
import net.fortuna.ical4j.util.RandomUidGenerator;
import net.fortuna.ical4j.validate.ValidationException;

/*
 Test URL : http://localhost:4502/content/gatewaytoanthem/api/generate-ics?eventTitle=Event Title&description=A brief event description&startDate=06-04-2021&startTime=09:22&endDate=06-04-2021&endTime=13:21&timezone=America/New_York&organiser=hello@gatewaytoanthem.com&location=Teams%20Meeting&meetingLink=https://somemeetinglink.com 
 */

@Component(service = Servlet.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=Generate an ICS File for a corresponding request payload..",
		"sling.servlet.resourceTypes=" + "anthem/generate-ics", 
		"sling.servlet.methods=" + HttpConstants.METHOD_GET })
public class ICSfileBuilderServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(ICSfileBuilderServlet.class);

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {

		System.setProperty("net.fortuna.ical4j.timezone.cache.impl", MapTimeZoneCache.class.getName());
		System.setProperty("net.fortuna.ical4j.timezone.update.enabled","false");

		EventDetails requestObj = fetchRequestPayload(request);
		Calendar icsCalObj = null;
		try {
			icsCalObj = buildCalendarObjectUsing(requestObj);
		} catch (URISyntaxException e) {
            LOG.error("Exception captured in ICSfileBuilder servlet {}",e);
		}		
		response.setContentType("text/calendar; charset=UTF-8");
	    final CalendarOutputter output = new CalendarOutputter();
	    try {
	      output.output(icsCalObj, response.getOutputStream());
	    } catch (final ValidationException ex) {
	    	LOG.error("Exception captured in ICSfileBuilder servlet {}",ex);
	    }

	}

	private EventDetails fetchRequestPayload(SlingHttpServletRequest request) throws IOException {

		EventDetails requestObj = new EventDetails();
		if (null != request.getParameter("eventTitle")) {
			requestObj.setEventTitle(URLDecoder.decode(request.getParameter("eventTitle"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("description")) {
			requestObj.setDescription(URLDecoder.decode(request.getParameter("description"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("startDate")) {
			requestObj.setStartDate(URLDecoder.decode(request.getParameter("startDate"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("startTime")) {
			requestObj.setStartTime(URLDecoder.decode(request.getParameter("startTime"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("endDate")) {
			requestObj.setEndDate(URLDecoder.decode(request.getParameter("endDate"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("endTime")) {
			requestObj.setEndTime(URLDecoder.decode(request.getParameter("endTime"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("timezone")) {
			requestObj.setTimezone(URLDecoder.decode(request.getParameter("timezone"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("organiser")) {
			requestObj.setOrganiser(URLDecoder.decode(request.getParameter("organiser"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("location")) {
			requestObj.setLocation(URLDecoder.decode(request.getParameter("location"), StandardCharsets.UTF_8.name()));
		}
		if (null != request.getParameter("meetingLink")) {
			requestObj.setMeetingLink(URLDecoder.decode(request.getParameter("meetingLink"), StandardCharsets.UTF_8.name()));
		}
		return requestObj;

	}

	private Calendar buildCalendarObjectUsing(EventDetails eventDetailsObj) throws URISyntaxException {

		TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
		TimeZone timezone = registry.getTimeZone(eventDetailsObj.getTimezone());
		VTimeZone tz = timezone.getVTimeZone();
				
		String description=eventDetailsObj.getDescription()
							+"\nJoin from PC, Mac, Linux, iOS or Android:"
							+eventDetailsObj.getMeetingLink();
		DateTime start = new DateTime(stringToDate(eventDetailsObj.getStartDate(),eventDetailsObj.getStartTime(),timezone));
		DateTime end = new DateTime(stringToDate(eventDetailsObj.getEndDate(),eventDetailsObj.getEndTime(),timezone));
		VEvent meeting = new VEvent();
		meeting.getProperties().add(new DtStart(start));
		meeting.getProperties().add(new DtEnd(end));
		meeting.getProperties().add(new Summary(eventDetailsObj.getEventTitle()));
		meeting.getProperties().add(new Description(description));
		meeting.getProperties().add(new Organizer(eventDetailsObj.getOrganiser()));
		meeting.getProperties().add(new Location(eventDetailsObj.getLocation()));
		meeting.getProperties().add(tz.getTimeZoneId());	
				
		RandomUidGenerator ug = new RandomUidGenerator();
		Uid uid = ug.generateUid();
		meeting.getProperties().add(uid);		

		Calendar icsCalendar = new Calendar();
		icsCalendar.getProperties().add(new ProdId("-//Google Inc//Google Calendar 70.9054//EN"));
		icsCalendar.getProperties().add(Version.VERSION_2_0);
		icsCalendar.getProperties().add(CalScale.GREGORIAN);
		icsCalendar.getComponents().add(meeting);
		return icsCalendar;
	}

	private Date stringToDate(String Date, String Time, TimeZone timezone) {
		String[] splitDate=Date.split("-");
		String[] splitTime=Time.split(":");
		int day=Integer.valueOf(splitDate[0]);
		int month=Integer.valueOf(splitDate[1]);
		int year=Integer.valueOf(splitDate[2]); 
		int hour=Integer.valueOf(splitTime[0]); 
		int minute=Integer.valueOf(splitTime[1]); 
		java.util.Calendar calendar = new GregorianCalendar();
		calendar.setTimeZone(timezone);
		calendar.set(year, month-1, day, hour, minute,0);
		LOG.debug("Event Date {}", calendar.getTime().toString());
		return calendar.getTime();
	}
	
}
