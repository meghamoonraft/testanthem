//package com.anthem.hrportal.core.servlets;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import java.io.IOException;
//
//import javax.jcr.RepositoryException;
//import javax.servlet.ServletException;
//
//import org.apache.commons.httpclient.HttpStatus;
//import org.apache.sling.api.resource.LoginException;
//import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
//import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
//import org.apache.sling.xss.XSSAPI;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//
//import com.anthem.hrportal.core.config.SupportFormConfigurationService;
//import com.anthem.hrportal.core.services.IEmailService;
//
//import io.wcm.testing.mock.aem.junit5.AemContext;
//import io.wcm.testing.mock.aem.junit5.AemContextExtension;
//
//@ExtendWith(AemContextExtension.class)
//public class ContactUsEmailServletTest {
//
//	public final AemContext context = new AemContext();
//
//	private MockSlingHttpServletRequest mockSlingHttpServletRequest;
//
//	private MockSlingHttpServletResponse mockSlingHttpServletResponse;
//
//	private static final String REQUEST_BODY_JSON_1 = "{\"fullName\":\"John Doe\",\"emailId\":\"hello@moonraft.com\",\"contactNumber\":\"123-2131232\",\"message\":\"Lorem ipsum\"}";
//
//	private static final String REQUEST_BODY_JSON_2 = "{\"fullNaame\":\"John Doe\",\"emailId\":\"hello@moonraft.com\",\"contactNumber\":\"123-2131232\",\"message\":\"Lorem ipsum\"}";
//
//	private static final String TO_ADDRESS = "emailid@somehost.com";
//
//	private static final String MAIL_SUBJECT = "REG : GatewayToAnthem [Support]";
//
//	private static final String BODY_TEMPLATE = "${fullName} ${emailId} ${contactNumber} ${message}";
//
//	private static final String BODY_CONTENT = "John Doe hello@moonraft.com 123-2131232 Lorem ipsum";
//
//	@InjectMocks
//	private final ContactUsEmailServlet contactUsServlet = new ContactUsEmailServlet();
//
//	@Mock
//	private transient IEmailService emailService;
//
//	@Mock
//	private transient SupportFormConfigurationService emailConfig;
//	
//	@Mock
//	private transient XSSAPI xssAPI;
//
//	@BeforeEach
//	public void setUp() throws LoginException, IOException {
//		MockitoAnnotations.initMocks(this);
//		mockSlingHttpServletRequest = context.request();
//		mockSlingHttpServletResponse = context.response();
//	
//		Mockito.when(emailConfig.getEmailToAddress()).thenReturn(TO_ADDRESS);
//		Mockito.when(emailConfig.getEmailSubject()).thenReturn(MAIL_SUBJECT);
//		Mockito.when(emailConfig.getEmailBodyTemplate()).thenReturn(BODY_TEMPLATE);
//		Mockito.when(emailService.sendEmail(MAIL_SUBJECT, BODY_CONTENT, TO_ADDRESS, "", "")).thenReturn(true);
//	}
//
//	@Test
//	void testSendMailSuccess() throws ServletException, IOException, RepositoryException {
//		mockSlingHttpServletRequest.setContentType("application/json");
//		mockSlingHttpServletRequest.setContent(REQUEST_BODY_JSON_1.getBytes());
//		contactUsServlet.doPost(mockSlingHttpServletRequest, mockSlingHttpServletResponse);
//		Mockito.verify(emailService).sendEmail(MAIL_SUBJECT, BODY_CONTENT, TO_ADDRESS, "", "");
//		Mockito.verify(emailConfig).getEmailToAddress();
//		Mockito.verify(emailConfig).getEmailSubject();
//		Mockito.verify(emailConfig).getEmailBodyTemplate();
//		assertEquals(HttpStatus.SC_OK, mockSlingHttpServletResponse.getStatus());
//	}
//
//	@Test
//	void testInvalidReqeustBody() throws ServletException, IOException, RepositoryException {
//		mockSlingHttpServletRequest.setContentType("application/json");
//		mockSlingHttpServletRequest.setContent(REQUEST_BODY_JSON_2.getBytes());
//		contactUsServlet.doPost(mockSlingHttpServletRequest, mockSlingHttpServletResponse);
//		assertEquals(HttpStatus.SC_BAD_REQUEST, mockSlingHttpServletResponse.getStatus());
//	}
//
//	@Test
//	void testSendMailFailure() throws ServletException, IOException, RepositoryException {
//		mockSlingHttpServletRequest.setContentType("application/json");
//		mockSlingHttpServletRequest.setContent(REQUEST_BODY_JSON_1.getBytes());
//		Mockito.when(emailService.sendEmail(MAIL_SUBJECT, BODY_CONTENT, TO_ADDRESS, "", "")).thenReturn(false);
//		contactUsServlet.doPost(mockSlingHttpServletRequest, mockSlingHttpServletResponse);
//		assertEquals(HttpStatus.SC_SERVICE_UNAVAILABLE, mockSlingHttpServletResponse.getStatus());
//	}
//
//	@Test
//	void testMissingTokenFromTemplate() throws ServletException, IOException, RepositoryException {
//		mockSlingHttpServletRequest.setContentType("application/json");
//		mockSlingHttpServletRequest
//				.setContent(REQUEST_BODY_JSON_1.replaceAll(",\"message\":\"Lorem ipsum\"", "").getBytes());
//		Mockito.when(
//				emailService.sendEmail(MAIL_SUBJECT, BODY_CONTENT.replaceAll("Lorem ipsum", ""), TO_ADDRESS, "", ""))
//				.thenReturn(true);
//		contactUsServlet.doPost(mockSlingHttpServletRequest, mockSlingHttpServletResponse);
//		assertEquals(HttpStatus.SC_OK, mockSlingHttpServletResponse.getStatus());
//	}
//
//	@Test
//	void testWithoutReqeustBody() throws ServletException, IOException, RepositoryException {
//		mockSlingHttpServletRequest.setContentType("application/json");
//		mockSlingHttpServletRequest.setContent(null);
//		contactUsServlet.doPost(mockSlingHttpServletRequest, mockSlingHttpServletResponse);
//		assertEquals(HttpStatus.SC_BAD_REQUEST, mockSlingHttpServletResponse.getStatus());
//	}
//
//}