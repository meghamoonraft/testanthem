package com.anthem.hrportal.core.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.anthem.hrportal.core.config.impl.SupportFormConfigurationServiceImpl;

import io.wcm.testing.mock.aem.junit5.AemContext;
import junitx.util.PrivateAccessor;

@ExtendWith(MockitoExtension.class)
public class SupportFormConfigurationServiceTest {
	
	public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);
	
	private static final String EMAIL_TO_ADDRESS = "dl@mail-host.com";

	private static final String EMAIL_SUBJECT = "REG : GatewayToAnthem [Support]";
	
	private static final String EMAIL_BODY_TEMPLATE ="${fullName} ${emailId} ${contactNumber} ${message}";
	
	private SupportFormConfigurationServiceImpl configUnderTest;
	
	@BeforeEach
	public void setUp() throws NoSuchFieldException {
		MockitoAnnotations.initMocks(this);
		configUnderTest = new SupportFormConfigurationServiceImpl();
		PrivateAccessor.setField(configUnderTest, "emailToAddress",EMAIL_TO_ADDRESS);
		PrivateAccessor.setField(configUnderTest, "emailSubject",EMAIL_SUBJECT);
		PrivateAccessor.setField(configUnderTest, "emailBodyTemplate",EMAIL_BODY_TEMPLATE);
	}
	
	@Test
	public void testEmailToAddress() {
		assertEquals(EMAIL_TO_ADDRESS,configUnderTest.getEmailToAddress());
		assertNotEquals("",configUnderTest.getEmailToAddress());	
	}
	
	@Test
	public void testEmailSubject() {
		assertEquals(EMAIL_SUBJECT,configUnderTest.getEmailSubject());
		assertNotEquals("",EMAIL_SUBJECT);	
	}
	
	@Test
	public void testEmailBodyTemplate() {
		assertEquals(EMAIL_BODY_TEMPLATE,configUnderTest.getEmailBodyTemplate());
		assertNotEquals("",EMAIL_BODY_TEMPLATE);	
	}


}
