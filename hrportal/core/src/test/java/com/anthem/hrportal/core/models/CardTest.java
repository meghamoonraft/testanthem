package com.anthem.hrportal.core.models;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;
import java.util.Date;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

@ExtendWith(AemContextExtension.class)
class CardTest {
		

			public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);

			private Card card;

			@BeforeEach
			public void setUp() {
				context.load().json("/Card.json", "/card");
				context.addModelsForClasses(Card.class);
				Resource componentResource = context.resourceResolver().getResource("/card");
				card = componentResource.adaptTo(Card.class);
				assertNotNull(card);
			}

			@Test
			public void testCardItems() {
				assertNotNull(card);
				Date date=Calendar.getInstance().getTime();
				  Card card = new Card();
				  card.setAltText("Alternate Text for the Image");
				  card.setEventImage("/content/dam/gatewaytoanthem/sample/eventdetails-cmp-copy.png");
				  card.setImage("/content/dam/gatewaytoanthem/eventdetails.jpg");
				  card.setPageTitle("Anthem");
				  card.setStartDate(date);
				  card.setPublishDate(date);
				  card.setStartTime(date);
				  card.setPath("/content/gatewaytoanthem/beacon/en/home");
				  card.setTag("gateway-to-anthem:events/virtual-event");
				  card.setTagIcon("/content/dam/gatewaytoanthem/event_flags.svg");
				  card.setAlt("alternate text");
				  card.setTagBackground("#cae0ef");
				  card.setVideo("/content/dam/gatewaytoanthem/event_flags.mp4");
				Assert.assertEquals(card.getImage(), "/content/dam/gatewaytoanthem/eventdetails.jpg");
				Assert.assertEquals(card.getStartDate(),date);
				Assert.assertEquals(card.getPublishDate(),date);
				Assert.assertEquals(card.getStartTime(),date);
				Assert.assertEquals(card.getImage(), "/content/dam/gatewaytoanthem/eventdetails.jpg");
				Assert.assertEquals(card.getAltText(), "Alternate Text for the Image");
				Assert.assertEquals(card.getEventImage(), "/content/dam/gatewaytoanthem/sample/eventdetails-cmp-copy.png");
				Assert.assertEquals(card.getPageTitle(), "Anthem");
				Assert.assertEquals(card.getPath(), "/content/gatewaytoanthem/beacon/en/home");
				Assert.assertEquals(card.getTag(), "gateway-to-anthem:events/virtual-event");
				Assert.assertEquals(card.getTagIcon(), "/content/dam/gatewaytoanthem/event_flags.svg");
				Assert.assertEquals(card.getAlt(),"alternate text");
				Assert.assertEquals(card.getTagBackground(),"#cae0ef");
				Assert.assertEquals(card.getVideo(),"/content/dam/gatewaytoanthem/event_flags.mp4");
				
			}
		

		}
