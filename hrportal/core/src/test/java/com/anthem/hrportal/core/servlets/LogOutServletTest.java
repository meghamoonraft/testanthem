package com.anthem.hrportal.core.servlets;

import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;

import javax.jcr.RepositoryException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith(AemContextExtension.class)
public class LogOutServletTest {

	public final AemContext context = new AemContext();

	private MockSlingHttpServletRequest mockSlingHttpServletRequest;

	private MockSlingHttpServletResponse mockSlingHttpServletResponse;

	public static final String LOGIN_TOKEN_COOKIE_LABEL = "login-token";

	public static final String LOGIN_TOKEN_COOKIE_VALUE = "d923a916-0e37-4841-bb94";

	@InjectMocks
	private final LogOutServlet logoutServlet = new LogOutServlet();

	@BeforeEach
	public void setUp() throws LoginException, IOException {
		
		MockitoAnnotations.initMocks(this);
		mockSlingHttpServletRequest = context.request();

		Cookie cookie = new Cookie(LOGIN_TOKEN_COOKIE_LABEL, LOGIN_TOKEN_COOKIE_VALUE);
		context.request().addCookie(cookie);

		mockSlingHttpServletResponse = context.response();
	}

	@Test
	void testLogoutServlet() throws ServletException, IOException, RepositoryException {
		logoutServlet.doGet(mockSlingHttpServletRequest, mockSlingHttpServletResponse);
		assertNull(mockSlingHttpServletResponse.getCookie(LOGIN_TOKEN_COOKIE_LABEL));
	}

}
