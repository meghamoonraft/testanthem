package com.anthem.hrportal.core.models;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

@ExtendWith(AemContextExtension.class)
class QuickLinksModelTest {
	
	public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);
	private QuickLinksModel quickLinksModel;
	
	@BeforeEach
	public void setUp () {
		context.addModelsForClasses(QuickLinksModel.class);
		context.load().json("/Quicklinks.json","/quicklinks");
		Resource componentResource=context.resourceResolver().getResource("/quicklinks");
		quickLinksModel = componentResource.adaptTo(QuickLinksModel.class);
		assertNotNull(quickLinksModel);
	}

	@Test
	public void testquickLinkItems() {
			assertNotNull(quickLinksModel.getQuickLinkItems());
			Assert.assertEquals(quickLinksModel.getQuickLinkItems().get(0).getQuickLinkTitle(), "Citrix");
			Assert.assertEquals(quickLinksModel.getQuickLinkItems().get(0).getQuickLinkImage(), "/content/dam/gatewaytoanthem/sample/citrix-icon-copy.png");
			Assert.assertEquals(quickLinksModel.getQuickLinkItems().get(0).getButtonCta(), "/content/gatewaytoanthem/beacon/en/home");
			Assert.assertEquals(quickLinksModel.getQuickLinkItems().get(0).getExternalLink(), "false");
			Assert.assertEquals(quickLinksModel.getQuickLinkItems().get(1).getLinkTarget(), "_blank");
		}

}
