package com.anthem.hrportal.core.models;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

@ExtendWith(AemContextExtension.class)
public class HeaderNavRefModelTest {

	public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);
	
	private static final String TEST_PAGE_PATH_EN="/content/gatewaytoanthem/mmm/en/timeline";
	private static final String ASSERTED_HEADER_PATH_EN="/content/gatewaytoanthem/mmm/en/admin-console/jcr:content/root/container/header";
	private static final String ASSERTED_NAV_PATH_EN="/content/gatewaytoanthem/mmm/en/admin-console/jcr:content/root/container/navigation";
	
	private static final String TEST_PAGE_PATH_ES="/content/gatewaytoanthem/mmm/es/timeline";
	private static final String ASSERTED_HEADER_PATH_ES="/content/gatewaytoanthem/mmm/es/admin-console/jcr:content/root/container/header";
	private static final String ASSERTED_NAV_PATH_ES="/content/gatewaytoanthem/mmm/es/admin-console/jcr:content/root/container/navigation";
	
	private static final String DEFAULT_HEADER_PATH="/content/gatewaytoanthem/beacon/en/admin-console/jcr:content/root/container/header";
	private static final String DEFAULT_NAV_PATH="/content/gatewaytoanthem/beacon/en/admin-console/jcr:content/root/container/navigation";
	
	@InjectMocks
	@Spy
	private HeaderAndNavigationRefModel headNavRefModel;

	@Mock
	Page page = mock(Page.class);
	PageManager pageManager = mock(PageManager.class);
	Resource contentResource = mock(Resource.class);
	ResourceResolver resourceResolver = mock(ResourceResolver.class);

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testHeaderAndNavRefPathForEn() {
		when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
		when(page.getContentResource()).thenReturn(contentResource);
		when(page.getPath()).thenReturn(TEST_PAGE_PATH_EN);
		headNavRefModel.init();
		Assert.assertEquals(headNavRefModel.getHeaderRefPath(),ASSERTED_HEADER_PATH_EN);
		Assert.assertEquals(headNavRefModel.getNavigationRefPath(),ASSERTED_NAV_PATH_EN);
	}
	
	@Test
	void testHeaderAndNavRefPathForEs() {
		when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
		when(page.getContentResource()).thenReturn(contentResource);
		when(page.getPath()).thenReturn(TEST_PAGE_PATH_ES);
		headNavRefModel.init();
		Assert.assertEquals(headNavRefModel.getHeaderRefPath(),ASSERTED_HEADER_PATH_ES);
		Assert.assertEquals(headNavRefModel.getNavigationRefPath(),ASSERTED_NAV_PATH_ES);
	}
	
	@Test
	void testIfPathPathIsEmpty() {
		when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
		when(page.getContentResource()).thenReturn(contentResource);
		when(page.getPath()).thenReturn("");
		headNavRefModel.init();
		Assert.assertEquals(headNavRefModel.getHeaderRefPath(),DEFAULT_HEADER_PATH);
		Assert.assertEquals(headNavRefModel.getNavigationRefPath(),DEFAULT_NAV_PATH);
	}

}
