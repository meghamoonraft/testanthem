package com.anthem.hrportal.core.models;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(AemContextExtension.class)
public class HeaderModelTest {

	public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);

	private HeaderModel headermodel;

	@BeforeEach
	public void setUp() {
		context.load().json("/HeaderModel.json", "/header");
		context.addModelsForClasses(HeaderModel.class);
		Resource componentResource = context.resourceResolver().getResource("/header");
		headermodel = componentResource.adaptTo(HeaderModel.class);
		assertNotNull(headermodel);
	}
	
	@Test
    public void testLanguage()  {
       Assert.assertEquals(headermodel.getLanguageDropDown().get(0).getLanguage(), "English");
       Assert.assertEquals(headermodel.getLanguageDropDown().get(1).getLanguage(), "Español");
    }
	
	@Test
    public void testLanguageKey()  {
       Assert.assertEquals(headermodel.getLanguageDropDown().get(0).getLanguageKey(), "en");
       Assert.assertEquals(headermodel.getLanguageDropDown().get(1).getLanguageKey(), "es");
    }

}
