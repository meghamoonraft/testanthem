package com.anthem.hrportal.core.models;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

@ExtendWith(AemContextExtension.class)
public class TimelineDetailModelTest {

	public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);

	private TimelineDetailModel timeLineDetailModel;

	@BeforeEach
	public void setUp() {
		context.load().json("/TimelineDetails.json", "/timeLineDetails");
		context.addModelsForClasses(HeaderModel.class);
		Resource componentResource = context.resourceResolver().getResource("/timeLineDetails");
		timeLineDetailModel = componentResource.adaptTo(TimelineDetailModel.class);
		assertNotNull(timeLineDetailModel);
	}

	@Test
	public void testMilestoneItems() {
		assertNotNull(timeLineDetailModel.getMilestoneListItems());
		Assert.assertEquals(timeLineDetailModel.getMilestoneListItems().get(0).getMilestoneTitle(), "ANTHEM ID's");
		Assert.assertEquals(timeLineDetailModel.getMilestoneListItems().get(0).getMilestoneLabel(), "Milestone");
		Assert.assertEquals(timeLineDetailModel.getMilestoneListItems().get(0).getMilestoneSubheading(), "Date : 27 February 2020");
		Assert.assertEquals(timeLineDetailModel.getMilestoneListItems().get(0).getMilestoneImage(), "/content/dam/gatewaytoanthem/sample/timeline-page-human-copy.png");
		Assert.assertEquals(timeLineDetailModel.getMilestoneListItems().get(1).getMilestoneLabel(), "Milestone");
	}

}
