package com.anthem.hrportal.core.services;

import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.mail.HtmlEmail;
import com.anthem.hrportal.core.services.impl.EmailServiceImpl;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.mailer.MessageGateway;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith(AemContextExtension.class)
public class EmailServiceTest {

	public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);

	@Spy
	@InjectMocks
	private EmailServiceImpl emailService = new EmailServiceImpl();

	@Mock
	private MessageGatewayService messageGatewayService;

	@Mock
	private MessageGateway<HtmlEmail> messageGatewayHtmlEmail;

	@BeforeEach
	public void setUp() throws NoSuchFieldException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(messageGatewayService.getGateway(HtmlEmail.class)).thenReturn(messageGatewayHtmlEmail);
		context.registerService(MessageGatewayService.class, messageGatewayService);
		context.registerInjectActivateService(emailService);
	}

	@Test
	public void testSuccessfullEmailSend() {
		String subject = "test-subject";
		String body = "test-body";
		String to = "random-mail@email.com";
		String cc = "";
		String bcc = "";
		boolean emailStatus = emailService.sendEmail(subject, body, to, cc, bcc);
		ArgumentCaptor<HtmlEmail> captor = ArgumentCaptor.forClass(HtmlEmail.class);
		Mockito.verify(messageGatewayHtmlEmail).send(captor.capture());
		assertTrue(emailStatus);
	}

	@Test
	public void testInavlidToAddress() {
		String subject = "test-subject";
		String body = "test-body";
		String to = "";
		String cc = "random-cc-mail@email.com";
		String bcc = "random-bcc-mail@email.com";
		boolean emailStatus = emailService.sendEmail(subject, body, to, cc, bcc);
		assertFalse(emailStatus);
	}
}