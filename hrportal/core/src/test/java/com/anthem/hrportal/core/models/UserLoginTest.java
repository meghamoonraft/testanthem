package com.anthem.hrportal.core.models;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

@ExtendWith(AemContextExtension.class)
public class UserLoginTest {
	
	public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);
	
	
	@InjectMocks
	@Spy
	private UserLogin userLogin;

	@Mock
	ResourceResolver resourceResolver = mock(ResourceResolver.class);
	
	
	@Mock
	private SlingHttpServletRequest slingHttpServletRequest;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	void testSuccessfullUserLogin() {
		when(slingHttpServletRequest.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.getUserID()).thenReturn("some-authenticated-user-id");
		userLogin.init();
		Assert.assertEquals(userLogin.getLoggedIn(), "true");
		Assert.assertNotNull(userLogin.getUserId());
	}
	
	@Test
	void testUnsuccessfullUserLogin() {
		when(slingHttpServletRequest.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.getUserID()).thenReturn("anonymous");
		userLogin.init();
		Assert.assertEquals(userLogin.getLoggedIn(), "false");
		Assert.assertNull(userLogin.getUserId());
	}

	@Test
	void testNonPublishEnvironment() {
		when(slingHttpServletRequest.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.getUserID()).thenReturn("anonymous");
		userLogin.init();
		Assert.assertEquals(userLogin.getLoggedIn(), "false");
		Assert.assertNull(userLogin.getUserId());
	}

}
