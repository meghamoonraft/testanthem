package com.anthem.hrportal.core.models;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

@ExtendWith(AemContextExtension.class)
public class ContactUsModelTest {
	

		public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);

		private ContactUsModel contactmodel;

		@BeforeEach
		public void setUp() {
			context.load().json("/ContactUs.json", "/contact");
			context.addModelsForClasses(ContactUsModel.class);
			Resource componentResource = context.resourceResolver().getResource("/contact");
			contactmodel = componentResource.adaptTo(ContactUsModel.class);
			assertNotNull(contactmodel);
		}
		
		@Test
	    public void testLinkedinIcon()  {
	      Assert.assertEquals(contactmodel.getContactUs().get(0).getLinkedinIcon(), "/content/dam/gatewaytoanthem/sample/linkedIn-icon.jpg");
	      Assert.assertEquals(contactmodel.getContactUs().get(1).getLinkedinIcon(), "/content/dam/gatewaytoanthem/sample/linkedIn-icon.jpg");
	      
		}
			@Test
		  public void testLinkedinLink()  {
			  Assert.assertEquals(contactmodel.getContactUs().get(0).getLinkedinLink(), "https://www.linkedin.com/home");
			  Assert.assertEquals(contactmodel.getContactUs().get(1).getLinkedinLink(), "https://www.linkedin.com/home");
		  }
			@Test
		  public void testAuthorName()  {
			  Assert.assertEquals(contactmodel.getContactUs().get(0).getAuthorName(), "John Doe");
			  Assert.assertEquals(contactmodel.getContactUs().get(1).getAuthorName(), "Jane Doe");
		  }
			@Test
		  public void testLetsConnect()  {
			  Assert.assertEquals(contactmodel.getContactUs().get(0).getLetsConnect(), "Let's connect");
			  Assert.assertEquals(contactmodel.getContactUs().get(1).getLetsConnect(), "Let Us Connect");
		  }
			@Test
		  public void testLinkedinAlt()  {
			  Assert.assertEquals(contactmodel.getContactUs().get(0).getLinkedinAlt(), "LinkedIn");
			  Assert.assertEquals(contactmodel.getContactUs().get(1).getLinkedinAlt(), "LinkedIn");
		  }
			@Test
		  public void testAuthorDesig()  {
			  Assert.assertEquals(contactmodel.getContactUs().get(0).getAuthorDesig(), "HR Manager");
			  Assert.assertEquals(contactmodel.getContactUs().get(1).getAuthorDesig(), "HR Sales");
		  }
			@Test
		  public void testAuthorImage()  {
			  Assert.assertEquals(contactmodel.getContactUs().get(0).getAuthorImage(), "/content/dam/gatewaytoanthem/quote_author_image.png");    
			  Assert.assertEquals(contactmodel.getContactUs().get(1).getAuthorImage(), "/content/dam/gatewaytoanthem/quote_author_image.png");    
		  }
	     
	   
		

	}