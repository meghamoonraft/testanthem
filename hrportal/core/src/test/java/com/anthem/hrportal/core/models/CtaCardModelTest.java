package com.anthem.hrportal.core.models;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

@ExtendWith(AemContextExtension.class)
class CtaCardModelTest {
	

		public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);

		private CtaCardModel ctaCard;

		@BeforeEach
		public void setUp() {
			context.load().json("/CtaCards.json", "/CtaCards");
			context.addModelsForClasses(CtaCardModel.class);
			Resource componentResource = context.resourceResolver().getResource("/CtaCards");
			ctaCard = componentResource.adaptTo(CtaCardModel.class);
			assertNotNull(ctaCard);
		}

		@Test
		public void testSelectVariation() {
			Assert.assertEquals(ctaCard.getSelectVariation(), "linksCard");
		}
		
		@Test
		public void testSimpleCardItems() {
			assertNotNull(ctaCard.getSimpleCard());
			Assert.assertEquals(ctaCard.getSimpleCard().get(0).getCardImage(), "/content/dam/gatewaytoanthem/footer.png");
			Assert.assertEquals(ctaCard.getSimpleCard().get(0).getCtaHeading(), "Get to know anthem");
			Assert.assertEquals(ctaCard.getSimpleCard().get(0).getCtaLink(), "/content/gatewaytoanthem/beacon/en/home");
			Assert.assertEquals(ctaCard.getSimpleCard().get(0).getCtaImageAlt(), "CTA card");
			Assert.assertEquals(ctaCard.getSimpleCard().get(0).getCtaDescription(), "You don't have to worry");
		}
		@Test
		public void testLinksCradItems() {
			assertNotNull(ctaCard.getLinksCard());
			Assert.assertEquals(ctaCard.getLinksCard().get(0).getCardImage(), "/content/dam/gatewaytoanthem/footer.png");
			Assert.assertEquals(ctaCard.getLinksCard().get(0).getCtaHeading(), "Get to know Anthem");
			Assert.assertEquals(ctaCard.getLinksCard().get(0).getCtaLink(), "/content/gatewaytoanthem/beacon/en/home");
			Assert.assertEquals(ctaCard.getLinksCard().get(0).getCtaImageAlt(), "Cta Card");
			Assert.assertEquals(ctaCard.getLinksCard().get(0).getCtaLabel(), "CTA Label");
		}

	}

