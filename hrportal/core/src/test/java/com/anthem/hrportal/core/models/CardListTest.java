package com.anthem.hrportal.core.models;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.*;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Style;

import io.wcm.testing.mock.aem.junit5.AemContext;
class CardListTest {
   private static final String PN_TAGS = "cq:tags";
   
   
   @Mock
    ValueMap properties;
    @Mock
    List<Card> cardlist;
    @Mock
    Style currentStyle;
    @Mock
    Page currentPage;
    @InjectMocks
    CardList cardList;
   @Mock
   private ValueMap vmap;
   

    private static final  String EVENT_RES_TYPE="anthemhr/hrportal/components/eventdetails";
   private static final  String ARTICLE_RES_TYPE="anthemhr/hrportal/components/article-title";
	private static final String CALENDAR_RES_TYPE="anthemhr/hrportal/components/calendar";
   private static final String IMAGE_RES_TYPE="anthemhr/hrportal/components/image";
   private static final String TAGS = "tags";
   String SOURCE = "listFrom";
   private static final String BRIGHTCOVE_RES_TYPE="anthemhr/hrportal/components/brightcove-player";

   public final AemContext context = new AemContext();
   private MockSlingHttpServletRequest request;
    
    @Mock
   Page page = mock(Page.class);
   PageManager pageManager = mock(PageManager.class);
   Resource contentResource = mock(Resource.class);
   ResourceResolver resourceResolver = mock(ResourceResolver.class);
   String path = "/content/gatewaytoanthem/beacon/en/home";


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetListType() {
        CardList.Source result = cardList.getListType();
        assertEquals(cardList.getListType(), result);
    }

    @Test
    void testGetPages() {
        List<Page> result = cardList.getPages();
        assertNotNull(cardList.getPages());
        assertEquals(cardList.getPages(), result);
    }

    @Test
    void testGetStaticListItems() {
       when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
       String[] path= {"/content/gatewaytoanthem/beacon/en/home"};
       when(this.properties.get("pages", new String[0])).thenReturn(path);
      //  Stream<Page> result = cardList.getStaticListItems();
      //  assertNotNull(cardList.getStaticListItems());
       cardList.getStaticListItems();
    }

    @Test
    void testGetChildListItems() {
       when(properties.get("childDepth", 1)).thenReturn(1);
       Optional<Page> rootPage1=Optional.ofNullable(page);
    }    
   
 
    @Test
    void testPostConstruct() {
       when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
      List<Page> pageList = new ArrayList<Page>();
      pageList.add(page); 
      when(page.getContentResource()).thenReturn(contentResource);
      when(page.getPath()).thenReturn(path); 
      when(page.getTitle()).thenReturn("Anthem");
  /*    String requestPath="somepath";     
      when(request.getRequestPathInfo().getResourcePath()).thenReturn(requestPath);
     final  Resource requestResource=mock(Resource.class);
     when(resourceResolver.getResource(requestPath)).thenReturn(requestResource);
     when(requestResource.getResourceType()).thenReturn(CALENDAR_RES_TYPE);
      //cardList.postConstruct();*/
    }
   @Test
    void testGetChildResourceImage() throws Exception {
       Card card=new Card();
       final String path = IMAGE_RES_TYPE;
       when(contentResource.getPath()).thenReturn(path);
       final Iterator<Resource> childResourceIterator = mock(Iterator.class);
       when(null !=childResourceIterator && childResourceIterator.hasNext()).thenReturn(true,false);
      when(contentResource.listChildren()).thenReturn(childResourceIterator);
      when(childResourceIterator.hasNext()).thenReturn(true, true, false);
      final Resource childResource = mock(Resource.class);
      when(childResourceIterator.next()).thenReturn(childResource);
      when(childResource.getResourceType()).thenReturn(IMAGE_RES_TYPE,"");
      final ValueMap vmap=mock(ValueMap.class);
      when(childResource.getValueMap()).thenReturn(vmap);
      when(vmap.get("fileReference",String.class)).thenReturn("test");
      when(vmap.get("alt",String.class)).thenReturn("test");
      when(childResource.hasChildren()).thenReturn(true);
      when(childResource.adaptTo(Card.class)).thenReturn(card);
      when(pageManager.getPage(path)).thenReturn(page);
      cardList.getChildResource(contentResource,path,path);
    }
   @Test
    void testGetChildResourceVideo() throws Exception {
       Card card=new Card();
       final String path =BRIGHTCOVE_RES_TYPE;
       when(contentResource.getPath()).thenReturn(path);
       final Iterator<Resource> childResourceIterator = mock(Iterator.class);
       when(null !=childResourceIterator && childResourceIterator.hasNext()).thenReturn(true);
      when(contentResource.listChildren()).thenReturn(childResourceIterator);
      when(childResourceIterator.hasNext()).thenReturn(true, true, false);
      final Resource childResource = mock(Resource.class);
      when(childResourceIterator.next()).thenReturn(childResource);
      when(childResource.getResourceType()).thenReturn(BRIGHTCOVE_RES_TYPE);
      final ValueMap vmap=mock(ValueMap.class);
      when(childResource.getValueMap()).thenReturn(vmap);
      when(vmap.get("account",String.class)).thenReturn("account");
      when(vmap.get("videoPlayer",String.class)).thenReturn("videoPlayer");
      when(childResource.hasChildren()).thenReturn(true);
      when(childResource.adaptTo(Card.class)).thenReturn(card);

      when(pageManager.getPage(path)).thenReturn(page);
      cardList.getChildResource(contentResource,path,path);
    }
   @Test
   void testGetChildResourceArticle() throws Exception {
      Card card=new Card();
      final String path = ARTICLE_RES_TYPE;
      when(contentResource.getPath()).thenReturn(path);
      final Iterator<Resource> childResourceIterator = mock(Iterator.class);
     when(contentResource.listChildren()).thenReturn(childResourceIterator);
     when(childResourceIterator.hasNext()).thenReturn(true, true, false);
     final Resource childResource = mock(Resource.class);
     when(childResourceIterator.next()).thenReturn(childResource);
     when(childResource.getResourceType()).thenReturn(ARTICLE_RES_TYPE);
     final ValueMap vmap=mock(ValueMap.class);
     when(childResource.getValueMap()).thenReturn(vmap);
     final String tag="tags";
     when(vmap.get(PN_TAGS, String.class)).thenReturn(tag);      
     when(TAGS.equals(properties.get(SOURCE, String.class))).thenReturn(true,false);
     when(properties.get(TAGS, String.class)).thenReturn(tag);
     when(childResource.hasChildren()).thenReturn(true);
     when(childResource.adaptTo(Card.class)).thenReturn(card);

     when(pageManager.getPage(path)).thenReturn(page);
     cardList.getChildResource(contentResource, path, path);        
   }
   @Test
   void testGetChildResourceEvent() throws Exception {
      Card card=new Card();
      final String path = EVENT_RES_TYPE;
      when(contentResource.getPath()).thenReturn(path);
      final Iterator<Resource> childResourceIterator = mock(Iterator.class);
     when(contentResource.listChildren()).thenReturn(childResourceIterator);
     when(childResourceIterator.hasNext()).thenReturn(true, true, false);
     final Resource childResource = mock(Resource.class);
     when(childResourceIterator.next()).thenReturn(childResource);   
     when(childResource.getResourceType()).thenReturn(EVENT_RES_TYPE+"/Else");
     final ValueMap vmap=mock(ValueMap.class);
     when(childResource.getValueMap()).thenReturn(vmap);
     final String tag="tags";
     when(vmap.get(PN_TAGS, String.class)).thenReturn(tag);     
     when(TAGS.equals(properties.get(SOURCE, String.class))).thenReturn(true,false);
     when(properties.get(TAGS, String.class)).thenReturn(tag);
     when(childResource.hasChildren()).thenReturn(true);
     when(childResource.adaptTo(Card.class)).thenReturn(card);

     when(pageManager.getPage(path)).thenReturn(page);
     cardList.getChildResource(contentResource,path,path);
   }
   
    @Test
    void testGetRootPage() {
        Optional<Page> result = cardList.getRootPage("fieldName");
        //Assertions.assertEquals(cardList.getRootPage("fieldName"), result);
    }

    @Test
    void testGetComponentdate() {
       Calendar date=Calendar.getInstance();
       Calendar time=Calendar.getInstance();
       when(cardList.getDateAndTime(date,time)).thenReturn(date);
        Calendar result = cardList.getComponentdate(contentResource, date);   
        assertEquals( cardList.getComponentdate(contentResource,date), result);
       }

}