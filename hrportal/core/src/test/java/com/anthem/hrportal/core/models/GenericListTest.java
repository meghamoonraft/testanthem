package com.anthem.hrportal.core.models;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;

@ExtendWith(AemContextExtension.class)
public class GenericListTest {
	
	public final AemContext context = new AemContext(ResourceResolverType.RESOURCERESOLVER_MOCK);

	private GenericList listObj;

	@BeforeEach
	public void setUp() {
		context.load().json("/GenericList.json", "/generic_list");
		context.addModelsForClasses(GenericList.class);
		Resource componentResource = context.resourceResolver().getResource("/generic_list");
		listObj = componentResource.adaptTo(GenericList.class);
		assertNotNull(listObj);
	}

	@Test
	public void testListItems() {
		assertNotNull(listObj.getGenericListItems());
	}
	
	@Test
	public void testItem() {
		Assert.assertEquals(listObj.getGenericListItems().get(0).getItemTitle(), "Health Benefit Election");
		Assert.assertEquals(listObj.getGenericListItems().get(0).getSubtitle(), "Getting Started");
		Assert.assertEquals(listObj.getGenericListItems().get(0).getItemLink(), "/content/gatewaytoanthem/beacon/en/home");
		assertTrue(listObj.getGenericListItems().get(0).getItemlinkTarget().equals("_blank")||
				   listObj.getGenericListItems().get(0).getItemlinkTarget().equals("_self"));
		assertTrue(listObj.getGenericListItems().get(0).getExternalLink().equals("true")||
				   listObj.getGenericListItems().get(0).getExternalLink().equals("false"));
		
		
	}


}
