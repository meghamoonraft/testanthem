package com.anthem.hrportal.core.servlets;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith(AemContextExtension.class)
public class ICSfileBuilderServletTest {

	public final AemContext context = new AemContext();

	private MockSlingHttpServletRequest mockSlingHttpServletRequest;

	private MockSlingHttpServletResponse mockSlingHttpServletResponse;

	private static final String CORRECT_QUERY_STRING = "eventTitle=Event Title&description=A brief event description&startDate=06-04-2021&startTime=09:22&endDate=06-04-2021&endTime=13:21&timezone=America/New_York&organiser=hello@gatewaytoanthem.com&location=Teams%20Meeting&meetingLink=https://somemeetinglink.com";

	@InjectMocks
	private final ICSfileBuilderServlet fileBuilderServlet = new ICSfileBuilderServlet();

	@BeforeEach
	public void setUp() throws LoginException, IOException {
		MockitoAnnotations.initMocks(this);
		mockSlingHttpServletRequest = context.request();
		mockSlingHttpServletResponse = context.response();
	}

	@Test
	void testFileBuilderServlet() throws ServletException, IOException, RepositoryException {
		context.request().setQueryString(CORRECT_QUERY_STRING);
		fileBuilderServlet.doGet(mockSlingHttpServletRequest, mockSlingHttpServletResponse);
		assertNotNull(mockSlingHttpServletResponse.getOutputAsString());
	}
}