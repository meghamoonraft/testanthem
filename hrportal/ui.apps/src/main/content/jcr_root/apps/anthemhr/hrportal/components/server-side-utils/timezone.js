use(function () {
    var timezoneShortCode = "";
    switch (this.timezone) {
        case "America/New_York":
            timezoneShortCode = "EST";
            break;
        case "America/Los_Angeles":
            timezoneShortCode = "PST";
            break;
        case "America/Chicago":
            timezoneShortCode = "CST";
            break;
        case "America/Denver":
            timezoneShortCode = "MDT";
            break;
        default:
            timezoneShortCode = "";
    }
    return {
        shortCode: timezoneShortCode
    };
});