$(function () {
  let dataLabel = document.getElementById("data-event-details");
  var copyLinkLabel = dataLabel.dataset.copylinklabel;
  var copiedLinkLabel = dataLabel.dataset.copiedlinklabel;
  var $shareIcon = $(
    ".detailswrap .detailswrap__cont .events__links .events__share__link .events__share "
  ); // DOM instance for share icon
  var $addCalender = $(
    ".detailswrap .detailswrap__cont .events__links .events__share__link .add__calendar"
  ); // DOM instance for add calender
  var $menuList = $(
    ".detailswrap .detailswrap__cont .events__links .events__options"
  );
  var $menuOptions = $(
    ".detailswrap .detailswrap__cont .events__links .events__share__link"
  );
  var $menuItems = $(
    ".detailswrap .detailswrap__cont .events__links .events__share__link .events__interact"
  ); //DOM instance for event details modal
  // Function to close the menu options on click of window
  var isMenuItemExpanded = false; // boolean variable to store the state of the dropdown
  $(document).on("click", function () {
    $menuOptions.hide();
    $menuList.removeClass("active");
  });
  // Function to restore the focus on parent element on closing the modal box
  $(document).on("keydown", function (event) {
    // condition to close the language dropdown on click of Esc key
    if (event.key == "Escape") {
      if (isMenuItemExpanded) {
        $menuList.focus();
        $("body").addClass("show-focus-outlines");
        isMenuItemExpanded = false;
      }
    }
  });
  // Function to toggle the options menu
  $menuList.on("click", function (event) {
    event.stopPropagation();
    isMenuItemExpanded = !isMenuItemExpanded;
    $(this).parent().find(".events__share__link").toggle();
    $(this).toggleClass("active");
    $(this)
      .parent()
      .find(".events__share__link")
      .find($shareIcon)
      .find("span:last-child")
      .text(copyLinkLabel);
  });
  // Function to handle the accessibility for menu options
  $menuOptions.find(".events__interact").on("keypress", function () {
    $(this).click();
  });
  // Function to enable arrow keys for language checkboxes
  $menuItems.on("keydown", function (event) {
    event.preventDefault();
    if (event.key == "ArrowUp") {
      if ($(this).is(":first-child")) {
        $(this).siblings().last().focus();
      } else {
        $(this).prev().focus();
      }
    } else if (event.key == "ArrowDown") {
      if ($(this).is(":last-child")) {
        $(this).siblings().first().focus();
      } else {
        $(this).next().focus();
      }
    } else if (event.key == "Tab") {
    } else if (event.key == "Enter") {
      $(this).click();
      $(this).focus();
    }
  });
  // Function to copy the meeting URL for clipboard
  $shareIcon.on("click", function (event) {
    event.stopPropagation();
    var copyText = document.createElement("input");
    var copyTextValue = window.location.href;
    document.body.appendChild(copyText);
    copyText.value = copyTextValue;
    copyText.select();
    document.execCommand("copy");
    document.body.removeChild(copyText);
    $(this).find("span:last-child").text(copiedLinkLabel);
  });

  $addCalender.click(function (event) {
    event.stopPropagation();
    var data = fetchEventDetailsPayload();
    //console.log("Payload for generate ics file servlet \n", JSON.stringify(data, null, 2));
    generateCalendarFile(data);
  });
  function fetchEventDetailsPayload() {
    let dataDom = document.getElementById("data-event-details");
    let reqPayload = {};
    reqPayload.eventTitle = dataDom.dataset.eventtitle
      ? encodeURI(dataDom.dataset.eventtitle)
      : "";
    reqPayload.description = dataDom.dataset.description
      ? encodeURI(dataDom.dataset.description)
      : "";
    reqPayload.startDate = dataDom.dataset.startdate
      ? encodeURI(dataDom.dataset.startdate)
      : "";
    reqPayload.startTime = dataDom.dataset.starttime
      ? encodeURI(dataDom.dataset.starttime)
      : "";
    reqPayload.endDate = dataDom.dataset.enddate
      ? encodeURI(dataDom.dataset.enddate)
      : "";
    reqPayload.endTime = dataDom.dataset.endtime
      ? encodeURI(dataDom.dataset.endtime)
      : "";
    reqPayload.timezone = dataDom.dataset.timezone
      ? encodeURI(dataDom.dataset.timezone)
      : "";
    reqPayload.organiser = dataDom.dataset.organiser
      ? encodeURI(dataDom.dataset.organiser)
      : "";
    reqPayload.location = dataDom.dataset.location
      ? encodeURI(dataDom.dataset.location)
      : "";
    reqPayload.meetingLink = dataDom.dataset.meetinglink
      ? encodeURI(dataDom.dataset.meetinglink)
      : "";
    return reqPayload;
  }

  function generateCalendarFile(requestData) {
    let fileName = decodeURIComponent(requestData.eventTitle)
      .toLowerCase()
      .replace(/ /g, "-")
      .replace(/[.]/g, "");
    $.ajax({
      url: "/content/gatewaytoanthem/api/generate-ics.html",
      method: "GET",
      dataType: "text",
      data: fetchEventDetailsPayload(),
      success: function (data) {
        var a = document.createElement("a");
        var url = window.URL.createObjectURL(
          new Blob([data], { type: "application/text" })
        );
        a.href = url;
        a.download = fileName + ".ics";
        document.body.append(a);
        a.click();
        a.remove();
        window.URL.revokeObjectURL(url);
      },
    });
  }
});
