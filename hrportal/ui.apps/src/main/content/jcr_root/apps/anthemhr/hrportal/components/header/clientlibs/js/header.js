$(function () {
  var $profileLogo = $(
    ".header__wrapper .header__right__block .header__profile__icon"
  ); // DOM instance for profile logo
  var $profileDropdown = $(
    ".header__wrapper .header__right__block .header__radio__btn__dropdown"
  ); // DOM instance for profile dropdown list
  var $languageList = $(
    ".header__wrapper .header__right__block .header__radio__btn__dropdown .language__dropdown"
  ); // DOM instance for language list
  var $toggleLanguageList = $(
    ".header__wrapper .header__right__block .header__radio__btn__dropdown .radio__btn__header"
  ); // DOM instance for language list toggle button
  var $toggleLanguageIcon = $(
    ".header__wrapper .header__right__block .header__radio__btn__dropdown .radio__btn__header .motif-icon"
  ); // DOM instance for language list toggle button icon
  var $headerHamburgerMenu = $(
    ".header__wrapper .header__left__block .motif-icon"
  ); // DOM instance for hamburger menu icon
  var $navigationItem = $(
    ".nav__container .cmp-navigation>.cmp-navigation__group>.cmp-navigation__item:first-child>.cmp-navigation__item-link"
  ); // DOM instance for first item in navigation menu

  let currentPath = window.location.pathname;
  let currentLangKey = "";
  let languageDomElem = $($languageList).find(".radio__btn");
  for (let i = 0, max = languageDomElem.length; i < max; i += 1) {
    let langKey = languageDomElem[i].id;
    if (currentPath.indexOf("/" + langKey + "/") !== -1) {
      $(languageDomElem[i]).prop("checked", true);
      currentLangKey = langKey;
    } else {
      $(languageDomElem[i]).prop("checked", false);
    }
  }
  // Function to handle the language selection
  $(".radio__btn__block").on("click", function (event) {
    event.stopPropagation();
    let selectedKey = $(this).find(".radio__btn").attr("id");
    let currentUrl = window.location.href;
    window.location.href = currentUrl.replace(
      "/" + currentLangKey + "/",
      "/" + selectedKey + "/"
    );
  });

  // Function to close the dropdown onclick anywhere in the window
  $(document).on("click", function () {
    $("body").removeClass("show-focus-outlines");
    if ($profileDropdown.hasClass("header__radio__btn__dropdown__show")) {
      $profileDropdown.toggleClass("header__radio__btn__dropdown__show");
      if ($languageList.css("display") == "block") {
        $languageList.toggle();
        $toggleLanguageIcon.toggleClass("dropdown__rot");
      }
    }
  });

  // Function to open/close the hamburger menu
  $headerHamburgerMenu.on("click", function () {
    $(this).toggleClass("motif-delete").toggleClass("motif-menu");
    toggleNavigationMenu();
  });

  // Function to toggle the navigation menu on click of hamburger menu
  function toggleNavigationMenu() {
    $("body .root").toggleClass("nav__visible");
    $("body .root").hasClass("nav__visible")
      ? $headerHamburgerMenu.attr("aria-expanded", true)
      : $headerHamburgerMenu.attr("aria-expanded", false);
    $(".full__width__container").toggleClass("padding--left--24");
    $(".anthem-layout-container--fixed").toggleClass("padding--left--24");
  }

  // Function to open the multi language select dropdown
  $profileLogo.on("click", function (event) {
    event.stopPropagation();
    if (
      $profileDropdown.hasClass("header__radio__btn__dropdown__show") &&
      $languageList.css("display") == "block"
    ) {
      $languageList.toggle();
      $toggleLanguageIcon.toggleClass("dropdown__rot");
    }
    $profileDropdown.toggleClass("header__radio__btn__dropdown__show");
  });

  // Function to handle the click of language dropdown option
  $toggleLanguageList.on("click", function (event) {
    event.stopPropagation();
    $($languageList).css("display") == "block"
      ? $toggleLanguageList.attr("aria-expanded", false)
      : $toggleLanguageList.attr("aria-expanded", true);
    $languageList.toggle();
    $toggleLanguageIcon.toggleClass("dropdown__rot");
  });

  // Logic to fix the minHeight for navigation and header wrapper
  var $navigationWrapper = $(
    "#hrportal__root > .aem-Grid--12 > .aem-GridColumn--default--2 > .cmp-container"
  ); // DOM instance for navigation wrapper
  var $mainContentWrapper = $(
    "#hrportal__root > .aem-Grid--12> .aem-GridColumn--default--10 > .cmp-container"
  ); // DOM instance for main wrapper
  $navigationWrapper.css("minHeight", $(window).height());
  if ($(window).width() <= 767) {
    $navigationWrapper.css("height", $(window).height());
    $mainContentWrapper.css("height", $(window).height());
  }

  // Functions for fixing the accessibility related issues

  // all the elements inside modal which is focusable
  const focusableElements =
    'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';

  const firstFocusableElement = $profileDropdown.find(focusableElements)[0]; // get first element to be focused inside modal
  const focusableContent = $profileDropdown.find(focusableElements);
  const lastFocusableElement = focusableContent[focusableContent.length - 1]; // get last element to be focused inside modal

  // Function to add the class for accessibility fix
  $(document).on("keydown", function (event) {
    if (event.key == "Tab") {
      $("body").addClass("show-focus-outlines");
      if (
        $profileDropdown.is(":visible") &&
        document.activeElement == lastFocusableElement
      ) {
        event.preventDefault();
        // if focused has reached to last focusable element then focus first focusable element after pressing tab
        firstFocusableElement.focus(); // add focus for the first focusable element
      }
    } else if (event.key == "Shift") {
      $("body").addClass("show-focus-outlines");
      if ($profileDropdown.is(":visible")) {
        event.preventDefault();
        // if focused has reached to last focusable element then focus first focusable element after pressing tab
        lastFocusableElement.focus(); // add focus for the first focusable element
      }
    }

    // condition to close the language dropdown on click of Esc key
    else if (event.key == "Escape") {
      var isProfileDropdownExpanded = $profileDropdown.hasClass(
        "header__radio__btn__dropdown__show"
      );
      $(document).click();
      if (isProfileDropdownExpanded) {
        $profileLogo.focus();
        $("body").addClass("show-focus-outlines");
      }
    }
  });

  // Function to handle the accessibility for hamburger menu
  $headerHamburgerMenu.on("keypress", function (event) {
    if (event.key == "Enter") {
      $headerHamburgerMenu.click();
      $navigationItem.focus();
    }
  });

  // Function to enable arrow keys for language checkboxes
  $(".radio__btn__block").on("keydown", function (event) {
    event.preventDefault();
    event.stopPropagation();
    if (event.key == "ArrowUp") {
      if ($(this).is(":first-child")) {
        $(this).siblings().last().focus();
      } else {
        $(this).prev().focus();
      }
    } else if (event.key == "ArrowDown") {
      if ($(this).is(":last-child")) {
        $(this).siblings().first().focus();
      } else {
        $(this).next().focus();
      }
    } else if (event.key == "Tab") {
      $(this).parent().next().find(focusableElements)[0].focus();
    } else if (event.key == "Enter") {
      $(this).click();
    } else if (event.key == "Escape") {
      $(document).click();
      $profileLogo.focus();
      $("body").addClass("show-focus-outlines");
    }
  });
});
