$(function () {
  $(".owl-carousel").owlCarousel({
    autoWidth: true,
    loop: false,
    nav: true,
    margin: 16,
    dots: false,
    rewind: true,
    navText: [
      '<span class="motif-icon motif-angle-left"></span>',
      '<span class="motif-icon motif-angle-right"> </span>',
    ],
    // Function to assign the aria label attribute for left and right nav buttons
    onInitialized: function () {
      $(".card__carousel .owl-carousel .owl-nav .owl-prev").attr(
        "aria-label",
        "slide the carousel left"
      );
      $(".card__carousel .owl-carousel .owl-nav .owl-next").attr(
        "aria-label",
        "slide the carousel right"
      );
    },
    responsive: {
      0: {
        items: 1,
      },
      452: {
        items: 2,
      },
      664: {
        items: 3,
      },
      768: {
        items: 2,
      },
      920: {
        items: 3,
      },
      1200: {
        items: 4,
      },
      1480: {
        items: 5,
      },
    },
  });
});
