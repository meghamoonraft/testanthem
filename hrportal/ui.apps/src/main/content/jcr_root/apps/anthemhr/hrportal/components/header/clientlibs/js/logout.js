$(function () {
  // DOM instance for logout button
  var $logout = $(
    ".header__wrapper .header__right__block .header__radio__btn__dropdown .radio__dropdown__logout .logout-button"
  );
  // Ajax call to clear the login cookie
  $logout.on("click", function () {
    $.ajax({
      type: "GET",
      url: "/content/gatewaytoanthem/api/logout.html",
      success: function () {
        window.location.href = $logout.attr("data-href");
      },
    });
  });
  // Function to handle the accessibility
  $logout.on("keypress", function (event) {
    if (event.key == "Enter") {
      $logout.click();
    }
  });
});
