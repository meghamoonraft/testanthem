$(function () {
    let currentHost = window.location.host;
    let hostList = [
        "www.gatewaytoanthem.com"
    ];
    if (hostList.indexOf(currentHost) >= 0) {
        $('a[href*="/content/gatewaytoanthem"]').each(function () {
            let updatedLinkPath = $(this)[0].href.replace("/content/gatewaytoanthem", "");
            $(this).attr("href", updatedLinkPath);
        });
    }
});