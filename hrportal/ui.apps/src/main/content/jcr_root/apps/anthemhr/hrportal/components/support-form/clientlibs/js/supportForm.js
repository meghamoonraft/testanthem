$(function () {
  //DOM Instance for all the input boxes in contact form.
  var $contactInputField = $(
    ".contact__form__container .contact__form__details__section .form__details__field input"
  );
  //DOM Instance for name input box in contact form.
  var $contactNameField = $(
    ".contact__form__container .contact__form__details__section .form__details__field input.form__details__full__name"
  );
  //DOM Instance for email input box in contact form.
  var $contactEmailField = $(
    ".contact__form__container .contact__form__details__section .form__details__field input.form__details__email"
  );
  //DOM Instance for phone number input box in contact form.
  var $contactPhoneField = $(
    ".contact__form__container .contact__form__details__section .form__details__field input.form__details__phone_no"
  );
  //DOM Instance for description input box in contact form.
  var $descriptionField = $(
    ".contact__form__container .contact__form__more__submit__section .tell__us__more__input"
  );
  //DOM instance for submit button in contact form.
  var $submitBtn = $(".contact__form__submit__btn");

  //DOM instance for the contact form.
  var $contactForm = $(".contact__form__container #contactForm");
  //DOM instance for contact form acknowledgement popup
  var $acknowledgePopup = $(".contact__form__ack__container");

  //DOM instance for contact form acknowledgement popup after successful form submission.
  var $acknowledgeSuccessPopup = $(
    ".contact__form__ack__container.support__success__dialog"
  );
  //DOM instance for contact form acknowledgement popup after unsuccessful form submission.
  var $acknowledgeErrorPopup = $(
    ".contact__form__ack__container.support__error__dialog"
  );

  //DOM instance for contact form acknowledgement popup close icon.
  var $ackPopupCloseIcon = $(
    ".contact__form__info .contact__info__crossicon .contact__cross__icon"
  );
  var $loader = $(".loader"); // DOM instance for loader section
  // POST API for submit button functionality
  var supportPostAPI = "/content/gatewaytoanthem/api/contact-us.json";

  //Function for Email validation using RegEx in contact form.
  function validateEmail() {
    var emailRegexValidation =
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (emailRegexValidation.test($contactEmailField.val())) {
      $contactForm.removeClass("error__form__submission");
      return true;
    } else {
      $contactForm.addClass("error__form__submission");
      return false;
    }
  }

  //Function for whether mandatory fields empty or not in contact form depending on which, submit button has to be active/disabled.
  function submitBtnState() {
    if (
      $contactNameField.val().length > 0 &&
      $contactEmailField.val().length > 0 &&
      $contactPhoneField.val().length > 4
    ) {
      $submitBtn.removeAttr("disabled");
    } else {
      $submitBtn.attr("disabled", "true");
    }
  }

  //Functionality on input focus in contact form.
  $contactInputField.on("focusin", function () {
    $(this).siblings(".contact__form__icon").css("color", "#286ce2");
    $(this).siblings(".form__details__label").css("display", "block");
    $contactForm.removeClass("error__form__submission");
  });

  //Functionality on input focusout in contact form.
  $contactInputField.on("focusout", function () {
    if ($(this).val().length > 0) {
      $(this).siblings(".contact__form__icon").css("color", "#286ce2");
      $(this).siblings(".form__details__label").css("display", "block");
    } else {
      $(this).siblings(".contact__form__icon").css("color", "#949494");
      $(this).siblings(".form__details__label").css("display", "none");
    }
  });

  // Calling function "submitBtnState()" on keyup of the mandatory input fields for btn active/disabled.
  $contactInputField.on("keyup", function () {
    submitBtnState();
  });

  //Functionality for Phone number input field '-' insertion after 3 digits and restricting input to ony numbers.
  $contactPhoneField.on("keypress", function (event) {
    if (/[0-9]/.test(event.key)) {
      if ($(this).val().length === 3) {
        this.value += "-";
      }
    } else {
      event.preventDefault();
    }
  });

  //Close Form acknowledgement on close icon click functionality if the form submission is successful.
  $acknowledgeSuccessPopup.find($ackPopupCloseIcon).on("click", function () {
    $acknowledgeSuccessPopup.hide();
    $contactForm[0].reset();
    $contactInputField.siblings(".contact__form__icon").css("color", "#949494");
    $contactInputField.siblings(".form__details__label").css("display", "none");
    $submitBtn.attr("disabled", "true");
    $acknowledgeSuccessPopup.attr("aria-hidden", false);
  });

  //Close Form acknowledgement on close icon click functionality if the form submission is unsuccessful.
  $acknowledgeErrorPopup.find($ackPopupCloseIcon).on("click", function (event) {
    event.stopPropagation();
    $acknowledgeErrorPopup.hide();
    $acknowledgeErrorPopup.attr("aria-hidden", false);
    $submitBtn.focus();
    $("body").addClass("show-focus-outlines");
  });

  //Submit button onclick fucntionality and email validation on button click.
  $submitBtn.on("click", function (e) {
    e.preventDefault();
    if (validateEmail()) {
      $loader.show();
      $.ajax({
        type: "POST",
        url: supportPostAPI,
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
          fullName: $contactNameField.val(),
          emailId: $contactEmailField.val(),
          contactNumber: $contactPhoneField.val(),
          message: $descriptionField.val(),
        }),
        // callback function to execute if the API call gets executed successfully
        success: function success(success) {
          $loader.hide();
          $acknowledgeSuccessPopup.css("display", "flex");
          $acknowledgeSuccessPopup.attr("aria-hidden", true);
        },
        // callback function to execute if the API call gets failed
        error: function error(error) {
          $loader.hide();
          $acknowledgeErrorPopup.css("display", "flex");
          $acknowledgeErrorPopup.attr("aria-hidden", true);
        },
      });
    } else {
      e.preventDefault();
    }
  });

  // Function to handle the accessibility fix
  $(document).on("keyup", function (event) {
    if (event.key == "Tab") {
      if ($acknowledgePopup.is(":visible")) {
        event.preventDefault();
        $ackPopupCloseIcon.focus();
      }
    } else if (event.key == "Escape") {
      if ($acknowledgePopup.is(":visible")) {
        event.preventDefault();
        $ackPopupCloseIcon.click();
      }
    }
  });
});
