use(function () {
     var tags =this.tag.split("/").pop();
    return {
        shortTag: tags.charAt(0).toUpperCase()+tags.slice(1).replace(/-/gi," ")
    };
});