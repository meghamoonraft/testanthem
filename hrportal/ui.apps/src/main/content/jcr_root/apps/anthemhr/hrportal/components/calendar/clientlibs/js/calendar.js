"use strict";

$(function () {
  // config for short month format
  var monthShortNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  // config for long day format
  var dayFullNames = {
    mon: "Monday",
    tue: "Tuesday",
    wed: "Wednesday",
    thu: "Thursday",
    fri: "Friday",
    sat: "Saturday",
    sun: "Sunday",
  };
  var $calenderList = $(".calender__list"); // DOM instance for calender list
  var defaultScrollEvents = null; // variable to store the info about the initial event to scroll on load
  var myCalendar = new VanillaCalendar({
    selector: "#myCalendar",
    date: new Date(),
    todaysDate: new Date(),
    onSelect: function onSelect(data) {
      var selectedDate = data.date.split(" ");
      var selectedDateFormat =
        selectedDate[2] + "-" + selectedDate[1] + "-" + selectedDate[3];
      scrollIntoEvents(selectedDateFormat);
    },
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    shortWeekday: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
  });
  configureCalenderButtons();
  appendEventsList(); // Function to configure the next/prev icons in the calender

  function configureCalenderButtons() {
    var $calendarPrevBtn = $(
      ".vanilla-calendar .vanilla-calendar-btn:first-child"
    );
    var $calendarNextBtn = $(
      ".vanilla-calendar .vanilla-calendar-btn:last-child"
    );
    $calendarPrevBtn.empty();
    $calendarPrevBtn.attr("aria-label", "Go to Previous month");
    $calendarPrevBtn.append(
      "<span class='motif-icon motif-angle-left'></span>"
    );
    $calendarNextBtn.empty();
    $calendarNextBtn.attr("aria-label", "Go to Next month");
    $calendarNextBtn.append(
      "<span class='motif-icon motif-angle-right'></span>"
    );
  } // Function to append the events fetched from the child events page

  function appendEventsList() {
    var calenderData = JSON.parse(
      $(".calender__data__value").attr("calender-data-events")
    )["result"];
    $calenderList.empty();

    for (
      eventItemIndex = 0;
      eventItemIndex < calenderData.length;
      eventItemIndex++
    ) {
      var startDate = calenderData[eventItemIndex]["startDate"].split(" ");
      var startTime = calenderData[eventItemIndex]["startTime"].split(" ");
      // Creating the date and time format for calender component
      var startDateFormat =
        startDate[2] + " " + startDate[1] + " " + startDate[5];
      var startTimeFormat = startTime[3].substr(0, 5);
      // Creating the date object for start Date to find the default list of events for auto scroll
      var todayDate = new Date();
      var startDateObject = new Date(
        startDate[5],
        monthShortNames.indexOf(startDate[1]),
        startDate[2]
      );

      if (
        todayDate.getFullYear() > startDateObject.getFullYear() ||
        (todayDate.getFullYear() == startDateObject.getFullYear() &&
          todayDate.getMonth() < startDateObject.getMonth()) ||
        (todayDate.getFullYear() == startDateObject.getFullYear() &&
          todayDate.getMonth() == startDateObject.getMonth() &&
          todayDate.getDate() <= startDateObject.getDate())
      ) {
        defaultScrollEvents == null
          ? (defaultScrollEvents = startDateFormat)
          : defaultScrollEvents;
      }

      if (
        $calenderList.find(
          "#calender__event__" + startDateFormat.replace(/ /g, "-")
        ).length > 0
      ) {
        // Create only the event list
        var eventTemplateString =
          '<a class="list__data" rel="noopener noreferrer" target="_blank" href='.concat(
            calenderData[eventItemIndex].path,
            '.html aria-label="'
              .concat(
                calenderData[eventItemIndex].pageTitle,
                '"><div class="list__events__info"><span class="events__date">'
              )
              .concat(startTimeFormat, '</span><span class="events__title">')
              .concat(
                calenderData[eventItemIndex].pageTitle,
                '</span></div><div class="list__events__redirect"><span class="motif-icon motif-right-arrow"></span></div></a></div></div>'
              )
          );
        $calenderList
          .find("#calender__event__" + startDateFormat.replace(/ /g, "-"))
          .find(".list__info")
          .append(eventTemplateString);
      } else {
        // Create a event list with parent wrapper
        var newTemplateString =
          '<div class="calender__list__item" id=calender__event__'
            .concat(
              startDateFormat.replace(/ /g, "-"),
              '><div class="list__header"><span class="list__title">'
            )
            .concat(
              todayDate.getDate() +
                " " +
                monthShortNames[todayDate.getMonth()] +
                " " +
                todayDate.getFullYear() ==
                startDateFormat
                ? "TODAY"
                : dayFullNames[startDate[0].toLowerCase()],
              '</span><span class="list__title__date">'
            )
            .concat(
              startDateFormat,
              '</span></div><div class="list__info"><a class="list__data" rel="noopener noreferrer" target="_blank" href='
            )
            .concat(
              calenderData[eventItemIndex].path,
              '.html aria-label="'
                .concat(
                  calenderData[eventItemIndex].pageTitle,
                  '"><div class="list__events__info"><span class="events__date">'
                )
                .concat(startTimeFormat, '</span><span class="events__title">')
                .concat(
                  calenderData[eventItemIndex].pageTitle,
                  '</span></div><div class="list__events__redirect"><span class="motif-icon motif-right-arrow"></span></div></a></div></div>'
                )
            );
        $calenderList.append(newTemplateString);
      }
    }
    // Function to auto scroll to the current events while loading of page
    defaultScrollEvents
      ? scrollIntoEvents(defaultScrollEvents.replace(/ /g, "-"))
      : scrollIntoEvents(defaultScrollEvents);
  }

  // Function to scroll the user to particular events based on the selection of date
  function scrollIntoEvents(selectedDate) {
    if (defaultScrollEvents) {
      if ($calenderList.find("#calender__event__" + selectedDate).length > 0) {
        var position =
          $calenderList.find("#calender__event__" + selectedDate).offset().top -
          $calenderList.offset().top +
          $calenderList.scrollTop();
        $calenderList.animate({
          scrollTop: position,
          speed: "slow",
        });
      }
    } else {
      $calenderList.animate({
        scrollTop: 1000000000,
        speed: "slow",
      });
      defaultScrollEvents = true;
    }
  }
});
