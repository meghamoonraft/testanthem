$(function () {
  checkValidUrl();
  const togglePwd = document.querySelector(
    ".login__wrapper #loginform .loginform__password #togglePassword"
  );
  const pwdValue = document.querySelector(
    ".login__wrapper #loginform .loginform__password #password"
  );
  const $inputField = $("#loginform .loginform__field input"); //DOM instance for input box
  togglePwd.addEventListener("click", togglePwdValue);
  // Function to handle the accessibility for hamburger menu
  togglePwd.addEventListener("keypress", function (event) {
    if (event.key == "Enter") {
      togglePwd.click();
    }
  });
  // Function to check the valid URL or not
  function checkValidUrl() {
    if (window.location.href.indexOf("?") > -1) {
      history.pushState("", document.title, window.location.pathname);
    }
  }
  // Function to add the class for accessibility fix
  $(document).on("keyup", function (event) {
    if (event.key == "Tab") {
      $("body").addClass("show-focus-outlines");
    }
  });
  // Function to remove the class for accessibility fix
  $(document).on("click", function () {
    $("body").removeClass("show-focus-outlines");
  });

  // Function to toggle the label while focus in or focus out of textBox
  $inputField.on("focus", function () {
    $(this).attr("placeholder", "");
    $(this).parents(".loginform__field").find("label").show();
    $(".anthem__login #errordiv").hide();
    $(this)
      .parents(".loginform__field")
      .find(".motif-icon")
      .addClass("input__focus");
  });

  $inputField.on("focusout", function () {
    $(this).attr("placeholder", $(this).attr("aria-label"));
    if ($(this).val().trim().length > 0) {
      $(this).parents(".loginform__field").find("label").show();
    } else {
      $(this).parents(".loginform__field").find("label").hide();
      $(this)
        .parents(".loginform__field")
        .find(".input__focus")
        .removeClass("input__focus");
    }
  });

  //[togglePwdValue] is used to show or hide the password
  function togglePwdValue() {
    // toggle the type attribute
    const type =
      pwdValue.getAttribute("type") === "password" ? "text" : "password";
    if (type === "text") {
      this.classList.remove("motif-invisible");
      this.classList.add("motif-visible");
    } else {
      this.classList.remove("motif-visible");
      this.classList.add("motif-invisible");
    }
    pwdValue.setAttribute("type", type);
  }

  // Function to click the login submit CTA from password field
  $("#password").keyup(function (event) {
    if (event.keyCode == 13) {
      $(".login__submit").click();
    }
  });
  // Function to handle the login submit CTA click
  $(".login__submit").click(function (e) {
    checkValidUrl();
    e.preventDefault();
    var valid = validateForm();
    if (valid) {
      $.ajax({
        type: "POST",
        url: $("#url").val(),
        data: {
          j_username: $("#username").val(),
          j_password: $("#password").val(),
          j_validate: "true",
        },
        success: function (data, textStatus, jqXHR) {
          window.location.href = getRedirectPath();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          $(".anthem__login #errordiv").css("display", "flex");
        },
      });
    } else {
      $(".anthem__login #errordiv").css("display", "flex");
    }
  });
  // Assigning the full screen height to login component
  $(".login__wrapper").css("minHeight", $(window).height());
});
// Function for validating Login form
var validateForm = function () {
  var valid = false;
  if ($("#username").val().length > 0 || $("#password").val().length > 0) {
    valid = true;
  }

  return valid;
};
//Function to redirect to homepage after login
var getRedirectPath = function () {
  redirectPath = $("#homePagePath").val() + ".html";
  return DOMPurify.sanitize(redirectPath);
};
