var maxNoOfItems = 5;
$(document).on("foundation-contentloaded", () => {
  let parentWrapperDom = document.querySelector(".anthem-carousel-editdialog-wrapper");
  let addButtonElement = parentWrapperDom.querySelector("._coral-Button--primary");
  setTimeout(() => {
    checkNumberOfItems(addButtonElement);
  }, 10);
  addListenerToDeleteBtn(addButtonElement);

  addButtonElement.addEventListener("click", () => {
    $(document).on("foundation-contentloaded", () => {
      checkNumberOfItems(addButtonElement);
      $(document).on("foundation-contentloaded", () => {
        addListenerToDeleteBtn(addButtonElement);
      });
    });
  });
});

function addListenerToDeleteBtn(element) {
  let parentDom = document.querySelector(".anthem-carousel-editdialog-wrapper");
  let deleteButtonElements = parentDom.querySelectorAll("._coral-Multifield-remove");
  deleteButtonElements.forEach((ele) => {
    ele.addEventListener("click", () => {
      setTimeout(() => {
        checkNumberOfItems(element);
      }, 10);
    });
  });
}

function checkNumberOfItems(element) {
  let parentDom = document.querySelector(".anthem-carousel-editdialog-wrapper");
  let itemLength = parentDom.querySelectorAll("coral-multifield-item").length;
  if (itemLength >= maxNoOfItems) {
    element.disabled = true;
  } else {
    element.disabled = false;
  }
}