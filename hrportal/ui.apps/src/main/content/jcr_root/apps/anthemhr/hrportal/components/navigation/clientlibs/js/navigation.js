$(function () {
  var $navigationRef = $(".navigation-ref"); // DOM instance for navigation reference component
  var $navigationContainer = $(".navigation-ref .nav__container"); // DOM instance for navigation container
  $navigationRef.css("minHeight", $(window).height());
  $navigationContainer.css("minHeight", $(window).height());
  // Fixing the container height for mobile view
  if ($(window).width() <= 767) {
    $navigationContainer
      .find(".cmp-navigation")
      .css("height", $(window).height() - 316);
  }
});
